import pymongo
from datetime import datetime, timedelta
import constants
import new_database_operations

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["PPAP_OEE"]
temp_production_orders = mydb["shift_records"]
shift_records = mydb["shift_records"]


def create_shift_report(start_time="12/11/2018", stop_time="13/11/2018"):
    start_timestamp = datetime.strptime((start_time + " " + constants.A_shift_start_time), "%d/%m/%Y %H:%M:%S")
    stop_timestamp = datetime.strptime((stop_time + " " + constants.A_shift_start_time), "%d/%m/%Y %H:%M:%S")

    current_shift_start_time = start_timestamp

    while current_shift_start_time < stop_timestamp:
        # ======= UPDATE SHIFT A   ========

        myquery = {"SHIFT DATE": current_shift_start_time.strftime("%d/%m/%Y") , "SHIFT ID": "A", }
        count = shift_records.find(myquery).count()

        if count == 0 :
            shift_record = {
                "TEAM LEADER": "",
                "TEAM MEMBERS": [],
                "GROUP LEADER": "",
                "DEPARTMENT HEAD": "",
                "SHIFT START TIME": current_shift_start_time,
                "SHIFT STOP TIME": current_shift_start_time + timedelta(minutes=constants.shift_duration),
                "SHIFT DATE": current_shift_start_time.strftime("%d/%m/%Y"),
                "SHIFT ID": "A",
                "PRODUCTION ORDERS": [],
            }
            shift_records.insert_one(shift_record)
        current_shift_start_time = current_shift_start_time + timedelta(minutes=constants.shift_duration)

        myquery = {"SHIFT DATE": current_shift_start_time.strftime("%d/%m/%Y"), "SHIFT ID": "B", }
        count = shift_records.find(myquery).count()
        if count == 0:
            shift_record = {
                "TEAM LEADER": "",
                "TEAM MEMBERS": [],
                "GROUP LEADER": "",
                "DEPARTMENT HEAD": "",
                "SHIFT START TIME": current_shift_start_time,
                "SHIFT STOP TIME": current_shift_start_time + timedelta(minutes=constants.shift_duration),
                "SHIFT DATE": current_shift_start_time.strftime("%d/%m/%Y"),
                "SHIFT ID": "B",
                "PRODUCTION ORDERS": [],
            }
            shift_records.insert_one(shift_record)
        current_shift_start_time = current_shift_start_time + timedelta(minutes=constants.shift_duration)

        myquery = {"SHIFT DATE": current_shift_start_time.strftime("%d/%m/%Y"), "SHIFT ID": "C", }
        count = shift_records.find(myquery).count()
        if count == 0:
            shift_record = {
                "TEAM LEADER": "",
                "TEAM MEMBERS": [],
                "GROUP LEADER": "",
                "DEPARTMENT HEAD": "",
                "SHIFT START TIME": current_shift_start_time,
                "SHIFT STOP TIME": current_shift_start_time + timedelta(minutes=constants.shift_duration),
                "SHIFT DATE": current_shift_start_time.strftime("%d/%m/%Y"),
                "SHIFT ID": "C",
                "PRODUCTION ORDERS": [],
            }
            shift_records.insert_one(shift_record)
        current_shift_start_time = current_shift_start_time + timedelta(minutes=constants.shift_duration)


if __name__ == '__main__':
    #create_shift_report(start_time="1/09/2020", stop_time="30/09/2020")
    create_shift_report(start_time="12/2/2021", stop_time="10/3/2021")

