import pandas
import numpy as np
import pymongo
import json

myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")

mydb1 = myclient1['PPAP_OEE']
prod_order_parameters_collection = mydb1['production_order_parameters']

''' drop collections '''
prod_order_parameters_collection.drop()

excel_data_df = pandas.read_excel('PartNumberDetails.xlsx', sheet_name='PartNumberDetails',index_col=None ,skiprows=[0])
excel_data_df = excel_data_df.replace(np.nan, '', regex=True)
excel_data_df = excel_data_df.replace('\n','', regex=True)

data_dict = json.loads(excel_data_df.to_json(orient='records'))

prod_order_parameters_collection = mydb1["production_order_parameters"]
prod_order_parameters_collection.insert_many(data_dict)

myquery1 = {}
mydoc1 = prod_order_parameters_collection.find(myquery1, {"_id": False})

for x1 in mydoc1:

    if "PART NUMBER" in x1:

        pm_prd_hr = x1["PM PRD/HR"]
        pm_prd_hr = int(pm_prd_hr)

        pm_prd_shift = x1["PM PRD/SHIFT"]
        pm_prd_shift = int(pm_prd_shift)

        sm_prd_hr = x1["SM PRD/HR"]
        sm_prd_hr = int(sm_prd_hr)

        sm_prd_shift = x1["SM PRD/SHIFT"]
        sm_prd_shift = int(sm_prd_shift)

        part_number = x1["PART NUMBER"]

        myquery2 = {'PART NUMBER': part_number}

        mydoc2 = prod_order_parameters_collection.find(myquery2, {'_id': False, "SAP PART NUMBER": True})

        sap_array = []

        for x2 in mydoc2:

            sap_part_no = x2["SAP PART NUMBER"]

            sap_array.append(sap_part_no)

        updated_fields = {"SAP": sap_array,
                          "PM PRD/HR": pm_prd_hr,
                          "PM PRD/SHIFT": pm_prd_shift,
                          "SM PRD/HR": sm_prd_hr,
                          "SM PRD/SHIFT": sm_prd_shift}

        updated_fields1 = {"$set": updated_fields}

        prod_order_parameters_collection.update_many(myquery2, updated_fields1)

prod_order_parameters_collection.update_many({}, {"$unset": {"SAP PART NUMBER": 1}})

prod_order_parameters_collection.update_many({}, {"$rename": {'SAP': 'SAP PART NUMBER'}})

prod_order_parameters_collection.update_many({}, {"$unset": {"LENGTH (mm)":1,
                                                             "WIDTH (mm)":2,
                                                             "HEIGHT (mm)":3,
                                                             "WEIGHT (kg)":4,
                                                             "Volume":5,
                                                             "BIN COLOR":6,
                                                             "Packing Standard":7,
                                                             "BIN COLOR HEX CODE":8,
                                                             "BIN Count":9,
                                                             "BIN No":10}})





















































































