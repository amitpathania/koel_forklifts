from datetime import datetime
import requests
import json

url = 'http://192.168.1.35/shot_data_post'
time_format = "%d/%m/%Y, %H:%M:%S"


f = open("record1.csv", "r")
data = f.read()
new_data = data.split("\n")

new_data = new_data[20:24]

to_send= {}
to_send["shot_details"] = []
for record in new_data :
    data = {}
    fields = record.split(",")
    #print(len(fields))
    if len(fields) > 4 :
        data["shot_count"] = str(fields[0])
        data['timestamp'] = str(fields[1]) +" "+  str(fields[2])
        data['timestamp'] =  datetime.strptime(data["timestamp"], "%m/%d/%Y %H:%M:%S")
        data['timestamp'] = data["timestamp"].strftime(time_format)
        data["cycle_time"] = str(fields[4])
        data["status"] = "Accept"
        to_send["shot_details"].append(data)

to_send["total_shots"] = len(to_send["shot_details"])
to_send["timestamp"] = datetime.now().strftime(time_format)
to_send["machine"] = "JSW 450T-III"
print(to_send)

headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post(url, data=json.dumps(to_send), headers=headers)
# extracting response text
pastebin_url = r.text
print("The pastebin URL is:%s" % pastebin_url)
