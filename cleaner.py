import pandas
import numpy as np
import pymongo
import json
from pprint import pprint
myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")

mydb1 = myclient1['PPAP_OEE']
prod_order_parameters_collection = mydb1['production_order_parameters']
partno_details_collection = mydb1['part_number_details']

''' drop collections '''
prod_order_parameters_collection.drop()
partno_details_collection.drop()

excel_data_df = pandas.read_excel('test_po_params_new.xlsx', sheet_name='Sheet1',index_col=None)
excel_data_df = excel_data_df.replace(np.nan, '', regex=True)
excel_data_df = excel_data_df.replace('\n','', regex=True)

data_dict = json.loads(excel_data_df.to_json(orient='records'))

partno_details_collection = mydb1["part_number_details"]
partno_details_collection.insert_many(data_dict)

prod_order_parameters_collection = mydb1['production_order_parameters']

myquery1 = {}
mydoc1 = partno_details_collection.find(myquery1, {"_id": False})

for x1 in mydoc1:

    if "CUSTOMER" in x1:
        customer = x1["CUSTOMER"]
    if "MODEL" in x1:
        model = x1["MODEL"]
    if "PART NAME" in x1:
        part_name = x1["PART NAME"]
    if "PART NUMBER" in x1:
        part_number = x1["PART NUMBER"]
    if "PART NUMBER (SAP)" in x1:
        sap_part_number = x1["PART NUMBER (SAP)"]
    if "PRIMARY MACHINE" in x1:
        primary_machine = x1["PRIMARY MACHINE"]
    if "PM MOULD CHANGE TIME (MIN)" in x1:
        pm_mould_chng_time = x1["PM MOULD CHANGE TIME (MIN)"]
    if "PM START UP TIME (MIN)" in x1:
        pm_start_up_time = x1["PM START UP TIME (MIN)"]
    if "PM START UP SCRAP(KG)" in x1:
        pm_start_up_scrap = x1["PM START UP SCRAP(KG)"]
    if "PM LINE REJECTION" in x1:
        pm_line_rejection = x1["PM LINE REJECTION"]
    if "PM CYCLE TIME(SEC)" in x1:
        pm_cycle_time = x1["PM CYCLE TIME(SEC)"]
    if "PM PRD/HR" in x1:
        pm_prd_hr = x1["PM PRD/HR"]
    if "PM PRD/SHIFT" in x1:
        pm_prd_shift = x1["PM PRD/SHIFT"]
    if "SECONDARY MACHINE" in x1:
        sec_machine = x1["SECONDARY MACHINE"]
    if "SM MOULD CHANGE TIME (MIN)" in x1:
        sm_mould_chng_time = x1["SM MOULD CHANGE TIME (MIN)"]
    if "SM START UP TIME (MIN)" in x1:
        sm_start_up_time = x1["SM START UP TIME (MIN)"]
    if "SM START UP SCRAP(KG)" in x1:
        sm_start_up_scrap = x1["SM START UP SCRAP(KG)"]
    if "SM LINE REJECTION" in x1:
        sm_line_rejection = x1["SM LINE REJECTION"]
    if "SM CYCLE TIME(SEC)" in x1:
        sm_cycle_time = x1["SM CYCLE TIME(SEC)"]
    if "SM PRD/HR" in x1:
        sm_prd_hr = x1["SM PRD/HR"]
    if "SM PRD/SHIFT" in x1:
        sm_prd_shift = x1["SM PRD/SHIFT"]


    fields = {"CUSTOMER": customer,
              "MODEL": model,
              "PART NAME": part_name,
              "PART NUMBER": part_number,
              "SAP PART NUMBER": sap_part_number,
              "PRIMARY MACHINE": primary_machine,
              "PM MOULD CHANGE TIME (MIN) ": pm_mould_chng_time,
              "PM START UP TIME (MIN)": pm_start_up_time,
              "PM START UP SCRAP(KG) ": pm_start_up_scrap,
              "PM LINE REJECTION": pm_line_rejection,
              "PM CYCLE TIME(SEC)": pm_cycle_time,
              "PM PRD/HR": pm_prd_hr,
              "PM PRD/SHIFT": pm_prd_shift,
              "SECONDARY MACHINE": sec_machine,
              "SM MOULD CHANGE TIME (MIN) ": sm_mould_chng_time,
              "SM START UP TIME (MIN)": sm_start_up_time,
              "SM START UP SCRAP(KG) ": sm_start_up_scrap,
              "SM LINE REJECTION": sm_line_rejection,
              "SM CYCLE TIME(SEC)": sm_cycle_time,
              "SM PRD/HR": sm_prd_hr,
              "SM PRD/SHIFT": sm_prd_shift
              }

    prod_order_parameters_collection.insert_one(fields)

#partno_details_collection.update_many({}, {"$unset": {"SAP PART NUMBER":1}})

'''
CLEANER CAN READ FORMAT FOUND IN test_po_params_2.xlsx

pass Overall DB Rev 4_new from read_database_oee

which creates a clean excel_file test_po_params_new.xlsx

pass test_po_params_new to cleaner.py





'''




































































































