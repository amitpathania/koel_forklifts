import pymongo
import json
import pytz
import datetime
from bson.json_util import dumps, loads
from bson import ObjectId, Binary, Code, json_util
import copy

myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")
# myclient1  = pymongo.MongoClient("mongodb://cyronics:cipl1234@35.184.20.184:27017/Anzen_1_0?authSource=Anzen_1_0")

import pyqrcode
#import png
from pyqrcode import QRCode
import string
import random
import uuid


mydb1 = myclient1['PPAP_OEE']
partno_details_collection = mydb1['part_number_details']
prod_order_parameters_collection = mydb1['production_order_parameters']

mydb2 = myclient1['PPAP_FLEET']
vehicle_details_collection = mydb2['Vehicle_details']

from random import randint
import datetime
import otp_operations
import fcm_message


def update_partnumber_details_database():

    partno_details_collection.drop()

    myquery1 = {}

    mydoc1 = prod_order_parameters_collection.find(myquery1, {"_id": False})

    for x1 in mydoc1:

        if "PART NUMBER" in x1:

            partno = x1["PART NUMBER"]

            count = partno_details_collection.find({"PART NUMBER": partno}).count()

            if count == 0:

                partno_details_collection.insert_one(x1)

    myquery2 = {}

    mydoc2 = prod_order_parameters_collection.find(myquery2, {"_id": False})

    for x2 in mydoc2:

        if "PART NUMBER" in x2:

            partno = x2["PART NUMBER"]

            count = partno_details_collection.find({"PART NUMBER": partno}).count()

            if count > 0:

                myquery3 = {'PART NUMBER': partno}

                x3 = vehicle_details_collection.find_one(myquery3, {'_id': False, "BIN No": False})

                count1 = vehicle_details_collection.find(myquery3, {'_id': False}).count()

                bincount = {"BIN Count": count1}

                mydoc3 = vehicle_details_collection.find(myquery3, {'_id': False})

                all_bin_nos = []

                for x4 in mydoc3:

                    if "BIN No" in x4:
                        binno = x4["BIN No"]

                        all_bin_nos.append(binno)

                binno_array = {"BIN No": all_bin_nos}

                if x3:

                    newfields = {"$set": x3}

                    partno_details_collection.update(myquery3, newfields)

                    bincountfield = {"$set": bincount}

                    partno_details_collection.update(myquery3, bincountfield)

                    binnoarrayfield = {"$set": binno_array}

                    partno_details_collection.update(myquery3, binnoarrayfield)

                else:

                    fields = {"LENGTH (mm)": "-",
                              "WIDTH (mm)": "-",
                              "HEIGHT (mm)": "-",
                              "WEIGHT (kg)": "-",
                              "Volume": "-",
                              "BIN COLOR": "-",
                              "Packing Standard": "-",
                              "BIN COLOR HEX CODE": "-",
                              "BIN Count": "0",
                              "BIN No": "-"}

                    newfields = {"$set": fields}

                    partno_details_collection.update(myquery3, newfields)


def query_partnumberdetails():

    myquery = {}

    mydoc = partno_details_collection.find(myquery, {'_id': False, "BIN No": False})

    all_x = []

    for x in mydoc:

        all_x.append(x)

    return all_x


def query_binnumberdetails(partno):

    myquery = {"PART NUMBER": partno}

    x = partno_details_collection.find_one(myquery, {'_id': False, "BIN No": True})

    return x["BIN No"]

























































































