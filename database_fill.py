import pymongo
import json
import bson
from bson.json_util import dumps,loads
from bson import ObjectId, Binary, Code,json_util
from datetime import datetime



myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")
mydb1 = myclient1['Anzen_1_0']
sign_up_collection = mydb1['Sign Up Questions']
question_data_collection = mydb1['all_question']

 

question_list=[

{"question":{
"que":"Email ID",
"type":"Single Entry",
"options":"",
"q_id":"question1",
"correct/better_option":"",
"sign_up":"yes",
"Daily_declaration":"no"}
},


{"question":{
"que":"Do you think Cyronics Software will help your organisation?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question2",
"correct/better_option":"",
"sign_up":"yes",
"Daily_declaration":"no"}
},

{"question":{
"que":"Do you think we should promote Smart Factory?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question3",
"correct/better_option":"",
"sign_up":"yes",
"Daily_declaration":"no"}
},

{"question":{
"que":"Do you think this will help in Paperless Operations?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question4",
"correct/better_option":"",
"sign_up":"yes",
"Daily_declaration":"no"}
},

{"question":{
"que":"Are you feeling nervous to use Cyronics Software for the firsttime?",
"type":"Options",
"options":["Yes","No"],
"q_id":"question5",
"correct/better_option":"",
"sign_up":"yes",
"Daily_declaration":"no"}
},

{"question":{
"que":"Any Other Information you wish to provide",
"type":"Single Entry",
"options":"",
"q_id":"question6",
"correct/better_option":"",
"sign_up":"yes",
"Daily_declaration":"no"}
},

]


def fill_question_db():
    question_data_collection.insert_many(question_list)
    print("question db ready....")
fill_question_db()