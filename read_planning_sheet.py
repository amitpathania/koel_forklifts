import pandas as pd
import pprint

import pandas
import numpy as np
import pymongo
import copy

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["PPAP_OEE"]
production_order_parameters = mydb["production_order_parameters"]


'''
Include all scripts
'''

'''import os
import sys
sys.path.append(os.path.join("..","Production Planning Sheets"))'''


def read_excel(filename,sheetname):
    try :
        #columns_to_parse = ['A','B','C','E','F ', 'G',"H","I","J","K","l","M","N","O","P","V","X","AC","AD","AF","AG"]
        excel_data_df = pandas.read_excel(filename, sheet_name=sheetname,skiprows=[0],index_col=None  )
    except :
        return "Sheet Name entered incorrectly. Please check !",None,None,None,None,None

    try :
        excel_data_df[["MACHINE"]] = excel_data_df[["MACHINE"]].fillna(method='ffill', axis=0)
        excel_data_df = excel_data_df.replace(np.nan, '', regex=True)
        excel_data_df = excel_data_df.replace('\n','', regex=True)

        #print(excel_data_df.columns.values.tolist())


        excel_data_df.rename(columns={
            "ORDER NUMBER": "ORDER NUMBER (RIGHT)_A",
            'Unnamed: 9': "ORDER NUMBER (RIGHT)_B",
            "Unnamed: 8": "ORDER NUMBER (LEFT)_A",
            'Unnamed: 10': "ORDER NUMBER (LEFT)_B",
            '''TIME\n(MINS) - N''': "TIME(MINS) - N",
            '''MAT. \nKGS.''': "MATERIAL KGS",
            '''PACK \nMODE''': "PACK MODE",
            '''PACK\nQTY''': "PACK QTY",
            "ACTUAL": "ACTUAL QTY",
            "New QUANTITY ": "NEW QTY",
            "PLAN QTY.": "PLAN QTY"
        }, inplace=True)

        excel_data_df = excel_data_df[[
            "MACHINE",
            "PRIORITY",
            "DATE",
            "TIME",
            "MODEL",
            "PART NUMBER",
            "ORDER NUMBER (RIGHT)_A",
            "ORDER NUMBER (RIGHT)_B",
            "ORDER NUMBER (LEFT)_A",
            "ORDER NUMBER (LEFT)_B",
            "PART NAME",
            "MATERIAL",
            "COLOR",
            "PLAN QTY",
            "ACTUAL QTY",
            "NEW QTY",
            "TIME(MINS) - N",
            "MATERIAL KGS",
            "PACK MODE",
            "DIMENSION",
            "PACK QTY",
            "PACK/HR"
        ]]

    except :
        return "Sheet structure Incorrect. Please Check !", None, None, None, None,None

    try:

        data = excel_data_df.to_dict(orient='record')

        data = data[1: len(data)]

        # Convert All part Numbers to String
        for specific_data in data:

            if specific_data["ORDER NUMBER (RIGHT)_A"] != "" and specific_data["ORDER NUMBER (RIGHT)_B"] != "" : 
                specific_data["ORDER NUMBER (RIGHT)"] = str(specific_data["ORDER NUMBER (RIGHT)_A"]) + "," + str(specific_data["ORDER NUMBER (RIGHT)_B"])
            
            elif specific_data["ORDER NUMBER (RIGHT)_A"] == "" and specific_data["ORDER NUMBER (RIGHT)_B"] != "" :
                specific_data["ORDER NUMBER (RIGHT)"] =  str(specific_data["ORDER NUMBER (RIGHT)_B"])
            
            elif specific_data["ORDER NUMBER (RIGHT)_A"] != "" and specific_data["ORDER NUMBER (RIGHT)_B"] == "":
                specific_data["ORDER NUMBER (RIGHT)"] = str(specific_data["ORDER NUMBER (RIGHT)_A"])

            else :
                specific_data["ORDER NUMBER (RIGHT)"] = ""

            if specific_data["ORDER NUMBER (LEFT)_A"] != "" and specific_data["ORDER NUMBER (LEFT)_B"] != "":
                specific_data["ORDER NUMBER (LEFT)"] = str(specific_data["ORDER NUMBER (LEFT)_A"]) + "," + str(
                    specific_data["ORDER NUMBER (LEFT)_B"])

            elif specific_data["ORDER NUMBER (LEFT)_A"] == "" and specific_data["ORDER NUMBER (LEFT)_B"] != "":
                specific_data["ORDER NUMBER (LEFT)"] = str(specific_data["ORDER NUMBER (LEFT)_B"])

            elif specific_data["ORDER NUMBER (LEFT)_A"] != "" and specific_data["ORDER NUMBER (LEFT)_B"] == "":
                specific_data["ORDER NUMBER (LEFT)"] = str(specific_data["ORDER NUMBER (LEFT)_A"])

            else:
                specific_data["ORDER NUMBER (LEFT)"] = ""

            del specific_data["ORDER NUMBER (RIGHT)_A"]
            del specific_data["ORDER NUMBER (RIGHT)_B"]
            del specific_data["ORDER NUMBER (LEFT)_A"]
            del specific_data["ORDER NUMBER (LEFT)_B"]

        backup_data = copy.deepcopy(data)
        print("FULL DATA - ", len(backup_data))
        '''# Iterate over key/value pairs in dict and print them
        for key, value in data[1].items():
            print(key, ' : ', value)'''

        # print(np.shape(data))
        # ANALYSIS

        '''
        Throw Error If NEW QTY. is empty or not float 
    
        Throw Error if PART NUMBER not in database 
    
        Throw Error if Primary and Secondary Machine Not in Database 
    
        '''
        '''
        Throw Error If NEW QTY. is empty or not float 
        '''
        errors = []
        index_errors = []
        index_error_part_numbers = []
        index_machine_errors = []

        for record in data:
            try:
                value = int(record["NEW QTY"])
            except:
                # record["error"] = "Incorrect Value of New Quantity. Please enter an Integer Value."
                errors.append(record)
                # data.pop(data.index(record))
                index_errors.append(data.index(record))
                # print(record)

        print("QUANTITY ERROR :", len(errors))

        '''
        Throw Error if Part Number not in database 
        '''
        errors_part_number = []
        counter1 = 0
        counter2 = 0

        for record in data:
            myquery = {"PART NUMBER": record["PART NUMBER"]}
            mydoc = production_order_parameters.find(myquery, {'_id': False}).count()
            if mydoc == 0:
                # record["error"] = "PART NUMBER NOT FOUND"
                errors_part_number.append(record)
                index_error_part_numbers.append(data.index(record))
                ##data.pop(data.index(record))
                counter1 = counter1 + 1
                # print(record)
            else:
                counter2 = counter2 + 1
                # print(record["PART NUMBER"])

        # print(counter1,counter2)
        print("PARTNUMBER ERROR :", len(errors_part_number))

        '''
        Throw Error if Primary and Secondary Machine Not in Database 
        '''

        errors_machine = []
        for record in data:
            error_flag = 0
            myquery = {"PART NUMBER": record["PART NUMBER"]}
            mydoc = production_order_parameters.find(myquery, {'_id': False}).count()
            if mydoc != 0:
                myquery = {"PART NUMBER": record["PART NUMBER"], "PRIMARY MACHINE": record["MACHINE"]}
                mydoc = production_order_parameters.find(myquery, {'_id': False}).count()
                if mydoc == 0:
                    error_flag = error_flag + 1

                myquery = {"PART NUMBER": record["PART NUMBER"], "SECONDARY MACHINE": record["MACHINE"]}
                mydoc = production_order_parameters.find(myquery, {'_id': False}).count()
                if mydoc == 0:
                    error_flag = error_flag + 1

                if error_flag == 2:
                    # record["error"] = "Machine and Part Number do not Match."
                    errors_machine.append(record)
                    # print(record["MACHINE"])
                    # data.pop(data.index(record))
                    index_machine_errors.append(data.index(record))

        good_index = []
        for i in range(0, len(data)):
            if i in index_machine_errors:
                pass
            elif i in index_errors:
                pass
            elif i in index_error_part_numbers:
                pass
            else:
                good_index.append(i)

        valid_data = []
        for index in good_index:
            data[index]["ACTION"] = "Start"
            valid_data.append(data[index])

        # print(valid_data[0])
        # print("Good Indexes" , good_index)

        print("MACHINE ERROR :", len(errors_machine))
        print("GOOD DATA :", len(valid_data))
        used_orders = []

        for record in valid_data:
            if record["MACHINE"].__contains__("JSW"):
                used_orders.append(record)
        print("Used DATA", len(used_orders))
        return "Imported Successfully!", valid_data, errors, errors_part_number, errors_machine, used_orders
    except:
        return "Parsing Error",None,None,None,None,None


if __name__ == '__main__':
    status,data, errors,errors_part_number,errors_machine,used_orders = read_excel("new_POS.xlsx","01")
    print(status)

