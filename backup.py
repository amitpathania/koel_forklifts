
def get_downtime_overlaps(order_details):
    correct_downtimes = []
    if "DOWNTIME" in order_details:
        # CONVERT TO TIMESTAMPS ,CHECK ONLY ACCEPTED DOWNTIMES , FILTER OUT BAD ONES OR EMPTY STRINGS
        downtimes = []
        for downtime in order_details["DOWNTIME"]:
            temp_downtime = copy.deepcopy(downtime)
            if temp_downtime["status"] == "Accept" and ("reasonlist" in temp_downtime):
                if temp_downtime["reasonlist"] != "Minor Stops" and temp_downtime["Mode"] != "Software":
                    try:
                        temp_downtime["start_time"] = datetime.strptime(temp_downtime["start_time"], constants.time_format)
                        temp_downtime["stop_time"] = datetime.strptime(temp_downtime["stop_time"], constants.time_format)
                        downtimes.append(temp_downtime)
                    except:
                        pass

        # Arrange Downtimes in ascending order of start time
        downtimes = sorted(downtimes,key=lambda x : (x['start_time']))

        print("=================================")
        print(order_details["ORDER NUMBER (LEFT)"])
        print("PRE - CLEANING DONWTIMES")
        for downtime in downtimes:
            #if "reasonlist" not in downtime:
            #    print(downtime)

            if downtime["reasonlist"] == "Mould Change":
                print(downtime)

        # FIND OVERLAPPING DOWNTIMES
        for i in range(len(downtimes)):
            start_time = downtimes[i]["start_time"]
            stop_time = downtimes[i]["stop_time"]
            if len(correct_downtimes) == 0:
                details = ""
                details = downtimes[i]["reasonlist"] + " - "
                details += downtimes[i]["start_time"].strftime(constants.time_format)
                details += " TO " + downtimes[i]["stop_time"].strftime(constants.time_format)
                downtimes[i]["details"] = details
                correct_downtimes.append(downtimes[i])

            else:
                flag_overlap = 0
                #print("=====================================")
                j = 0
                k = -1
                while j < len(correct_downtimes):
                #for j in range(len(correct_downtimes)):
                    #print(j)
                    to_check_start_time = correct_downtimes[j]["start_time"]
                    to_check_stop_time = correct_downtimes[j]["stop_time"]
                    if j == k :
                        j = j + 1
                    elif to_check_start_time < start_time and to_check_stop_time < start_time:
                        j = j + 1
                    elif to_check_start_time > stop_time and to_check_stop_time > stop_time:
                        j = j + 1
                    elif to_check_start_time == start_time and to_check_stop_time == stop_time:
                        j = j + 1
                        flag_overlap += 1 # ignore completely
                    else:
                        #print("Downtime Overlap Occured")
                        #print(downtime["reasonlist"] ,to_check_start_time,to_check_stop_time )
                        #print(j,"=>",correct_downtimes[j]["reasonlist"],start_time,stop_time)
                        k = j
                        temp = {}

                        # GET THE MINIMUM OF START TIME AND MAXIMUM OF STOP TIME
                        if to_check_start_time < start_time:
                            temp["start_time"] = to_check_start_time
                        else:
                            temp["start_time"] = start_time
                        if to_check_stop_time > stop_time:
                            temp["stop_time"] = to_check_stop_time
                        else:
                            temp["stop_time"] = stop_time


                        if "reasonlist" in correct_downtimes[j]:
                            temp["reasonlist"] = correct_downtimes[j]["reasonlist"]
                        else :
                            temp["reasonlist"] = "NA"

                        # Append to reasonlists
                        if downtimes[i]["Mode"] == "Operator Input":
                            temp["reasonlist"] = downtimes[i]["reasonlist"]
                            #print("CHANGED REASON -" ,downtimes[i]["reasonlist"] )

                        '''if downtimes[i]["reasonlist"] != "Software" :
                            temp["reasonlist"] = downtimes[i]["reasonlist"]'''

                        temp["Mode"] = "Software"
                        temp["status"] = "Accept"
                        details = downtimes[i]["reasonlist"] + " - "
                        details += downtimes[i]["start_time"].strftime(constants.time_format)
                        details += " TO " + downtimes[i]["stop_time"].strftime(constants.time_format)
                        temp["details"] = correct_downtimes[j]["details"] + " , " + details



                        correct_downtimes[j] = temp



                        flag_overlap += 1
                        j = 0

                #print("=====================================")

                if flag_overlap == 0:
                    details = ""
                    details = downtimes[i]["reasonlist"] + " - "
                    details += downtimes[i]["start_time"].strftime(constants.time_format)
                    details += " TO " + downtimes[i]["stop_time"].strftime(constants.time_format)
                    downtimes[i]["details"] = details
                    correct_downtimes.append(downtimes[i])
                    #print("NO OVERLAP OCCURED. ADDING")
                    #print(downtime["reasonlist"], to_check_start_time, to_check_stop_time)
                    k = -1
                #print("TOTAL LENGTH = ", len(correct_downtimes))

        for downtime in correct_downtimes:
            if downtime["status"] == "Accept":
                try:
                    downtime["start_time"] = downtime["start_time"].strftime(constants.time_format)
                    downtime["stop_time"] = downtime["stop_time"].strftime(constants.time_format)
                    downtimes.append(downtime)
                except:
                    pass

    print("POST - CLEANING DONWTIMES")
    for i in range(len(correct_downtimes)):
        #if "reasonlist" not in downtime :
        #    print(downtime)
        if correct_downtimes[i]["reasonlist"] == "Mould Change":

            print(i," => ",correct_downtimes[i])
    print(correct_downtimes)
    return correct_downtimes
