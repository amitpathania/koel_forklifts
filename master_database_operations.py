import pymongo
import json
import pytz
import datetime
from bson.json_util import dumps, loads
from bson import ObjectId, Binary, Code, json_util
import copy

myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")
mydb1 = myclient1['Client_Name']
sign_up_collection = mydb1['Sign Up Questions']
import pyqrcode
import png
from pyqrcode import QRCode
import string
import random
import uuid

user_collection = mydb1['Master_Users']
visitors_collection = mydb1['Visitors']
questions_collection = mydb1['Contact Us']

from random import randint
import datetime
import otp_operations


def get_signup_questions():
    myquery = {}
    mydoc = sign_up_collection.find(myquery, {'_id': False})
    questions = []
    for x in mydoc:
        questions.append(x)
    return questions


def signup_user(data):
    myquery = {"phone": data["phone"]}
    mydoc = user_collection.find(myquery, {'_id': False}).count()
    if mydoc == 0:

        data["otp"] = randint(100000, 999999)
        data["uid"] = ""
        data["epass"] = {

        }
        data["sign_up_questions"] = {
            "respiratory": data["respiratory"],
            "diabetes": data["diabetes"],
            "heart": data["heart"],
            "bp": data["bp"],
            "none": data["none"],
            "yes_age": data["yes_age"],
        }
        data["sign_up_status"] = {
            "application_status": "Rejected",
            "application_timestamp": datetime.datetime.now() + datetime.timedelta(minutes=330)
        }
        # data["declaration_history"]:[]
        data["attendance_history"] = []
        data["contact_trace"]: []
        user_collection.insert_one(data)
        visitors_collection.insert_one(copy.deepcopy(data))
        # otp_operations.send_otp(data["otp"])

        return True
    else:
        return False


def return_otp(phone):
    myquery = {"phone": str(phone)}
    mydoc = user_collection.find(myquery, {'otp': True, "_id": False})
    otp = []
    for x in mydoc:
        otp.append(x)
    if len(otp) > 0:
        return otp[0]
    else:
        return 0


def return_credentials(phone):
    myquery = {"phone": str(phone)}
    mydoc = user_collection.find(myquery, {'name': True, 'password': True, 'role': True, 'o_id': True, "_id": False})
    credentials = []
    for x in mydoc:
        credentials.append(x)
    if len(credentials) > 0:
        return credentials[0]
    else:
        return 0


def verify_credentials(data):
    myquery = {"phone": str(data["phone"])}
    mydoc = user_collection.find(myquery,
                                 {'password': True, 'role': True, 'o_id': True, 'sign_up_status': True, "_id": False})
    credentials = []
    for x in mydoc:
        credentials.append(x)
    if len(credentials) > 0:
        if data["password"] == credentials[0]["password"] and data["role"] == credentials[0]["role"]:
            if credentials[0]["sign_up_status"]['application_status'] == "Accepted":
                return 2
            else:
                return 1
        else:
            return 0
    else:
        return 0


def update_otp(phone):
    myquery = {"phone": phone}
    mydoc = user_collection.find(myquery, {'_id': False}).count()
    if mydoc > 0:
        otp = randint(100000, 999999)
        myquery = {"phone": phone}
        newvalues = {"$set": {"otp": otp, "timestamp": datetime.datetime.now() + datetime.timedelta(minutes=330)}}
        user_collection.update_one(myquery, newvalues)

        otp_operations.send_otp(otp)

        return True
    else:
        return False


def reset_password(data):
    myquery = {"phone": data['phone']}
    mydoc = user_collection.find(myquery, {'_id': False}).count()
    if mydoc > 0:
        password = data['password']
        myquery = {"phone": data['phone']}
        newvalues = {
            "$set": {"password": password, "timestamp": datetime.datetime.now() + datetime.timedelta(minutes=330)}}
        user_collection.update_one(myquery, newvalues)
        return True
    else:
        return False


def id_generator(size=12, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def update_declaration(phone, declaration):
    temp_dict = {}
    temp_dict["declaration_questions"] = {
        "fever": declaration["fever"],
        "cough": declaration["cough"],
        "breathing": declaration["breathing"],
        "redzone": declaration["redzone"],
    }
    temp_dict["timestamp"] = datetime.datetime.now() + datetime.timedelta(minutes=330)
    uid = str(uuid.uuid1())
    if declaration["fever"] == "no" and declaration["cough"] == "no" and declaration["breathing"] == "no" and \
            declaration["redzone"] == "no":

        qrcode = pyqrcode.create(uid)
        qrcode.png('code/' + uid + '.png', scale=12)

        temp_dict["uid"] = uid
        temp_dict["epass_status"] = "Accepted"
        temp_dict["qrcode_link"] = "/epass/" + uid + '.png'
    else:
        temp_dict["uid"] = uid
        temp_dict["epass_status"] = "Rejected"
        temp_dict["qrcode_link"] = "/epass/reject.svg"

    myquery = {"phone": phone}
    newvalues = {"$set": {"epass": temp_dict, "uid": uid}}
    user_collection.update_one(myquery, newvalues)

    myquery = {"phone": phone}
    mydoc = user_collection.find(myquery, {'declaration_history': True, "_id": False})
    history = []
    for x in mydoc:
        history.append(x)
    # print(history)
    # print(history)
    if "declaration_history" in history[0]:
        history[0]['declaration_history'].append(temp_dict)
        newvalues = {"$set": {'declaration_history': history[0]['declaration_history']}}
        user_collection.update_one(myquery, newvalues)
        # print("Appending")
        # print(history)
    else:
        history = [temp_dict]
        myquery = {"phone": phone}
        newvalues = {"$set": {'declaration_history': history}}
        user_collection.update_one(myquery, newvalues)
        # print("Creating_New")


def get_epass(phone):
    myquery = {"phone": phone}
    mydoc = user_collection.find(myquery, {'epass': True, "_id": False})

    epass = []
    for x in mydoc:
        epass.append(x)
    print("------PRINTING EPASS------------")
    print(epass[0])
    return epass[0]['epass']


# -----------------------------------------------Attendance---------------------------------------------------
def attendance_data(o_id):
    current_time = datetime.datetime.now() + datetime.timedelta(minutes=330)
    date = current_time.strftime("%d/%m/%Y")
    data_to_return = []
    myquery = {"o_id": o_id}
    data = user_collection.find(myquery, {'_id': False, "name": True, "phone": True, "attendance_history": True,
                                          "declaration_history": True})

    # default_declarations
    all_data = []
    for temp_data in data:
        all_data.append(temp_data)

    # print(all_data)
    for user_data in all_data:
        temp_arr = []
        name = user_data["name"]
        phone = user_data["phone"]
        if "declaration_history" in user_data:
            epass_status = "No"
            for declaration in user_data["declaration_history"]:
                if "timestamp" in declaration:
                    if declaration["timestamp"].strftime("%d/%m/%Y") == date:
                        epass_status = "Yes"
            attendance_status = "No"
            temp = " - "
            for attendance_data in user_data["attendance_history"]:
                if attendance_data["timestamp"].strftime("%d/%m/%Y") == date:
                    attendance_status = "Yes"
                    temp = str(attendance_data["temperature"])
            temp_arr.append(name)
            temp_arr.append(epass_status)
            temp_arr.append(
                "<button class='btn btn-sm btn-primary' data-toggle='modal' data-target='#myModal' value=" + phone + "  onclick = 'modal_fun(this)'>GO</button>")
            temp_arr.append(temp)
            temp_arr.append(attendance_status)
            data_to_return.append(temp_arr)
        else:
            pass

    return data_to_return


def attendance_modal(data, o_id):
    date = data["date"]
    myquery = {"o_id": o_id, "phone": str(data["number"])}
    data = user_collection.find(myquery, {'_id': False, "name": True, "phone": True,
                                          "declaration_history": True})

    # default_declarations
    all_data = []
    for temp_data in data:
        all_data.append(temp_data)

    data = {'breathing': "",
            'cough': "",
            'fever': "",
            'redzone': ""}

    # print(all_data)
    for user_data in all_data:

        if "declaration_history" in user_data:

            for declaration in user_data["declaration_history"]:
                if "declaration_questions" in declaration:
                    if declaration["timestamp"].strftime("%d/%m/%Y") == date:
                        data = declaration["declaration_questions"]
    return data


def date_wise_attendance(date, o_id):
    # current_time = datetime.datetime.now() + datetime.timedelta(minutes=330)
    # date = current_time.strftime("%d/%m/%Y")
    data_to_return = []
    myquery = {"o_id": o_id}
    data = user_collection.find(myquery, {'_id': False, "name": True, "phone": True, "attendance_history": True,
                                          "declaration_history": True})

    # default_declarations
    all_data = []
    for temp_data in data:
        all_data.append(temp_data)

    # print(all_data)
    for user_data in all_data:
        temp_arr = []
        name = user_data["name"]
        phone = user_data["phone"]
        if "declaration_history" in user_data:
            epass_status = "No"
            for declaration in user_data["declaration_history"]:
                if "timestamp" in declaration:
                    if declaration["timestamp"].strftime("%d/%m/%Y") == date:
                        epass_status = "Yes"
            attendance_status = "No"
            temp = " - "
            for attendance_data in user_data["attendance_history"]:
                if attendance_data["timestamp"].strftime("%d/%m/%Y") == date:
                    attendance_status = "Yes"
                    temp = str(attendance_data["temperature"])
            temp_arr.append(name)
            temp_arr.append(epass_status)
            temp_arr.append(
                "<button class='btn btn-sm btn-primary' data-toggle='modal' data-target='#myModal' value=" + phone + "  onclick = 'modal_fun(this)'>GO</button>")
            temp_arr.append(temp)
            temp_arr.append(attendance_status)
            data_to_return.append(temp_arr)
        else:
            pass

    return data_to_return


# --------------------------------------------------end-Attendance------------------------------------


# --------------------------------------------------Visitors-------------------------------------------
def visitor_data(o_id):
    temp = visitors_collection.find({"o_id": o_id})
    data_to_return = []
    for i in temp:
        temp_arr = []
        temp_arr.append(i["name"])
        temp_arr.append(i["epass_status"])
        attribute_data = i["phone"]
        temp_arr.append(
            "<button class='btn btn-sm btn-primary' data-toggle='modal' data-target='#myModal' value=" + attribute_data + "  onclick = 'modal_fun(this)'>GO</button>")
        temp_arr.append(i["temperature"])
        dt = datetime.datetime.now()
        db_arr = i["attendance_timestamp"]

        for db_dt in db_arr:
            if db_dt.strftime("%d/%m/%Y") == dt.strftime("%d/%m/%Y"):
                temp_arr.append("Yes")
            else:
                temp_arr.append("No")
        data_to_return.append(temp_arr)

    return data_to_return


def visitor_modal(no, o_id):
    i = visitors_collection.find_one({"o_id": o_id, "phone": str(no)})
    data = {'breathing': i["breathing"], 'cough': i["cough"], 'fever': i["fever"], 'redzone': i["redzone"]}
    return data


def date_wise_visitor(date, o_id):
    data_to_return = []
    data = visitors_collection.find({"o_id": o_id})
    for i in data:
        for j in i["attendance_timestamp"]:
            if j.strftime("%d/%m/%Y") == date:
                temp_arr = []
                temp_arr.append(i["name"])
                temp_arr.append(i["epass_status"])
                attribute_data = i["phone"]
                temp_arr.append(
                    "<button class='btn btn-sm btn-primary' data-toggle='modal' data-target='#myModal' value=" + attribute_data + "  onclick = 'modal_fun(this)'>GO</button>")
                temp_arr.append(i["temperature"])
                temp_arr.append("Yes")
                data_to_return.append(temp_arr)
    return data_to_return


# --------------------------------------------------end-Visitors------------------------------------


# --------------------------------------------------Application---------------------------------------
def application_data(o_id):
    temp = user_collection.find({"o_id": o_id})
    data_to_return = []
    for i in temp:
        temp_arr = []
        temp_arr.append(i["name"])
        temp_arr.append(i["phone"])
        temp_arr.append('<select class="form-control form-control-sm wd-150" data-phone="' + i["phone"] + '" onchange="role_change(this)" id="role">\
			   <option value="' + i["role"] + '" selected disabled> ' + i["role"] + ' </option>\
				<option value="Employee">Employee</option>\
				  <option value="Security">Security</option>\
				  <option value="Admin" >Admin</option>\
		</select>')
        attribute_data = i["phone"]
        temp_arr.append(
            "<button class='btn btn-sm btn-primary' data-toggle='modal' data-target='#myModal' value=" + attribute_data + "  onclick = 'modal_fun(this)'>GO</button>")
        if i["sign_up_status"]["application_status"] == "Accepted":
            temp_arr.append(
                '<button class="btn btn-sm btn-success" onclick="acc_rej(this)" value =' + attribute_data + '>' +
                i["sign_up_status"]["application_status"] + '</button>')
        else:
            temp_arr.append(
                '<button class="btn btn-sm btn-danger" onclick="acc_rej(this)" value =' + attribute_data + '>' +
                i["sign_up_status"]["application_status"] + '</button>')
        data_to_return.append(temp_arr)
    return data_to_return


def change_status(arr, o_id):
    user_collection.update({"o_id": o_id, "phone": str(arr[0])},
                           {'$set': {"sign_up_status.application_status": arr[1]}})
    return application_data(o_id)


def modal_application(no, o_id):
    i = user_collection.find_one({"o_id": o_id, "phone": str(no)})
    data = {'respiratory': i["respiratory"], 'diabetes': i["diabetes"], 'heart': i["heart"], 'bp': i["bp"],
            'yes_age': i["yes_age"], "none": i["none"]}
    return data


def update_role(arr, o_id):
    user_collection.update({"o_id": o_id, "phone": str(arr[0])}, {'$set': {"role": arr[1]}})
    return "ok"


# --------------------------------------------------end-Application------------------------------------
# --------------------------------------------------Trace-------------------------------------------

def trace_data(input, o_id):
    count = 0
    data = None
    if input["searchby"] == "name":
        myquery = {"o_id": o_id, "name": input["search"]}
        data = user_collection.find_one(myquery, {"contact_trace": True})
    if input["searchby"] == "number":
        myquery = {"o_id": o_id, "phone": input["search"]}
        data = user_collection.find_one(myquery, {"contact_trace": True})
    if data == None:
        return False, 0
    else:
        data_to_return = []
        if "contact_trace" in data:
            for i in data["contact_trace"]:
                temp_dict = {}
                temp_dict["name"] = i["name"]
                temp_dict["number"] = i["number"]
                dt = i["timestamp"]
                temp_dict["timestamp"] = dt.strftime("%d/%m/%Y")
                print(temp_dict)
                data_to_return.append(temp_dict)
            return True, data_to_return


# ==========================================
# add visitor code

def visitor_request(dt):
    print("dt received = ", dt)
    # query to check if visitor number already in db
    # if yes , append
    # if no , create new document

    # send visitor epass via twilio


def retrive_user_temp(user):
    myquery = {'phone': user}
    user_dt = user_collection.find_one(myquery, {'_id': False, 'declaration_history': 1})
    temperature_arr = []
    temperature_arr.append(["Date", "Temperature", "Fever"])
    # print(user_dt)
    if "declaration_history" in user_dt:
        decl_list = user_dt['declaration_history']
        # print (decl_list)
        for j in decl_list:
            if 'scan_details' in j:
                date = (j['scan_details']['timestamp']).strftime('%d /%m /%Y')
                user_temp = j['scan_details']['temperature']
                high_end = 38.0
                temp = [date, user_temp, high_end]
                temperature_arr.append(temp)
    return temperature_arr


# ==========================================
def log_question(question, session):
    print("Loggin Question")
    data = {
        "question": question,
        "organisation": session["o_id"],
        "name": session["name"],
        "phone": session["user"],
        "timestamp": datetime.datetime.now() + datetime.timedelta(minutes=330),
        "attended_by": "",
        "attended_timestamp": "",
        "mode": "",
        "squash": ""
    }
    questions_collection.insert_one(data)


def manual_temp_entry(data, session):
    myquery = {"o_id": session['o_id'], "phone": data["number"]}
    print(session["o_id"])
    print(data["number"])
    data1 = user_collection.find_one(myquery, {'_id': False, "name": True, "declaration_history": True})
    if data1 == None:
        print("No data found")
        return False, True
    else:
        temperature_ok = True
        if data["temperature"] > 38.0:
            temperature_ok = False

        # -------------------Update Attendance ------------------------
        print("-------------")
        myquery = {"o_id": session['o_id'], "phone": data["number"]}
        attendance_data = user_collection.find_one(myquery, {'_id': False, "attendance_history": True})
        print(attendance_data)
        attendance_data = attendance_data['attendance_history']
        current_time = datetime.datetime.now() + datetime.timedelta(minutes=330)

        attendance_found = 0

        for todays_attendance in attendance_data:
            if "timestamp" in todays_attendance:
                # if attendance is already marked for today then update temp
                if current_time.strftime("%d/%m/%Y") == todays_attendance["timestamp"].strftime("%d/%m/%Y"):
                    attendance_found = 1
                    todays_attendance["temperature"] = data["temperature"]
                else:
                    pass

        if attendance_found == 0:
            attendance_data.append({
                "timestamp": current_time,
                "temperature": data["temperature"]
            })

        myquery = {"phone": data["number"]}
        # print(dec_history)
        newvalues = {"$set": {"attendance_history": attendance_data}}
        user_collection.update_one(myquery, newvalues)

        print("--------------")
        # ------------- Update  Attendance Over ----------------------------

        mask = True
        if data["mask"] == "no":
            mask = False

        scan_details = {
            "temperature": data["temperature"],
            "timestamp": datetime.datetime.now() + datetime.timedelta(minutes=330),
            "security_incharge": session['name'],
            "mask": mask,
            "temperature_ok": temperature_ok
        }
        to_add = {
            "manual_scan": "True",
            "scan_details": scan_details
        }
        # print(data1)
        dec_history = data1["declaration_history"]
        # print(dec_history)
        dec_history.insert(0, to_add)

        myquery = {"phone": data["number"]}
        newvalues = {"$set": {"declaration_history": dec_history}}
        user_collection.update_one(myquery, newvalues)
        print("Updated")
        return True, temperature_ok


def scanned_security(data, session):
    myquery = {"o_id": session['o_id'], "uid": data["uid"]}
    print(session["o_id"])
    data1 = user_collection.find_one(myquery, {'_id': False, "name": True, "declaration_history": True})
    if data1 == None:
        print("No data found")
        return False, True
    else:
        temperature_ok = True
        if data["temperature"] > 38.0:
            temperature_ok = False

        # -------------------Update Attendance ------------------------
        print("-------------")
        myquery = {"o_id": session['o_id'], "uid": data["uid"]}
        attendance_data = user_collection.find_one(myquery, {'_id': False, "attendance_history": True})
        print(attendance_data)
        attendance_data = attendance_data['attendance_history']
        current_time = datetime.datetime.now() + datetime.timedelta(minutes=330)

        attendance_found = 0

        for todays_attendance in attendance_data:
            if "timestamp" in todays_attendance:
                # if attendance is already marked for today then update temp
                if current_time.strftime("%d/%m/%Y") == todays_attendance["timestamp"].strftime("%d/%m/%Y"):
                    attendance_found = 1
                    todays_attendance["temperature"] = data["temperature"]
                else:
                    pass

        if attendance_found == 0:
            attendance_data.append({
                "timestamp": current_time,
                "temperature": data["temperature"]
            })

        myquery = {"uid": data["uid"]}
        # print(dec_history)
        newvalues = {"$set": {"attendance_history": attendance_data}}
        user_collection.update_one(myquery, newvalues)

        print("--------------")
        # ------------- Update  Attendance Over ----------------------------
        mask = True
        if data["mask"] == "no":
            mask = False

        dec_history = data1["declaration_history"]
        scan_details = {
            "temperature": data["temperature"],
            "timestamp": datetime.datetime.now() + datetime.timedelta(minutes=330),
            "security_incharge": session['name'],
            "mask": mask,
            "temperature_ok": temperature_ok
        }
        for single_declaration in dec_history:
            if "uid" in single_declaration:
                if single_declaration["uid"] == data["uid"]:
                    single_declaration["scan_details"] = scan_details

        myquery = {"uid": data["uid"]}
        # print(dec_history)
        newvalues = {"$set": {"declaration_history": dec_history}}
        user_collection.update_one(myquery, newvalues)
        # print("Updated")
        return True, temperature_ok


def get_number_from_uid(uid, session):
    myquery = {"o_id": session['o_id'], "uid": uid}
    data1 = user_collection.find_one(myquery, {'_id': False, "phone": True})
    if data1 == None:
        print("No data found")
        return False, 0
    else:
        return True, data1["phone"]


