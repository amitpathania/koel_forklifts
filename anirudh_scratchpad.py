import pymongo
myclient1 = pymongo.MongoClient("mongodb://localhost:27017/")
mydb1 = myclient1['Client_Name']
user_collection = mydb1['Users']


data = {
        "o_id": "Cyronics",
        "name": "Siddharth Bhonge",
        "password": "123456",
        "phone": "9922998224",
        "age": "25",
        "role": "Employee",
        "respiratory": "no",
        "diabetes": "no",
        "heart": "no",
        "bp": "no",
        "none": "yes",
        "yes_age": "no",
        "application_status": "no",
        "otp": 891031,
        "timestamp": {
            "date": "2020-05-24T08:14:44.450Z"
        },
        "epass_status": "Accepted",
        "temperature": 35.2,
        "attendance_timestamp": {
            "date": "2020-05-24T08:30:05.548Z"
        },
        "declaration_history": [
            {
                "fever": "no",
                "cough": "no",
                "breathing": "no",
                "redzone": "no",
                "status": "Accepted",
                "qrcode": "/epass/Q1ZUQH0VO0I4.png",
                "timestamp_qrcode": {
                    "date": "2020-05-24T08:30:05.548Z"
                },
                "uid": "Q1ZUQH0VO0I4",
                "scan": {
                    "secuirty_incharge": "Siddharth Bhonge",
                    "temperature": 35.2,
                    "timestamp": {
                        "date": "2020-05-24T08:30:05.548Z"
                    }
                }
            }
           
        ],
        "uid": "62XAZ7ZPFAX8"
    }



user_collection.insert(data)