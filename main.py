from pymongo import MongoClient

import constants
#from gevent import monkey
import csv
from pprint import pprint
#monkey.patch_all()
import copy
import requests
from flask import Flask, render_template, url_for, send_file, jsonify, request, session, send_from_directory, redirect
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_session import Session
import time
import datetime
from werkzeug.middleware.proxy_fix import ProxyFix
import json

import uuid
import rest_api

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'
app.config['SESSION_TYPE'] = 'mongodb'
# app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
Session(app)
# socketio = SocketIO(app, manage_session=False)
# app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1)
socketio = SocketIO(app, manage_session=False, cors_allowed_origins="*")

import database_operations

import part_number_details

import otp_operations
import threading

import new_database_operations

import read_planning_sheet

planning_sheet_save_path = "../Planning Sheets Upload/"
SPR_path = "SPR"
import os

'''
@app.after_request
def add_header(r):

    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    # r.headers['Cache-Control'] = 'public, max-age=0'
    r.headers['X-Content-Type-Options'] = 'nosniff'
    r.headers['X-Frame-Options'] = 'SAMEORIGIN'

    return r'''

@socketio.on("connected")
def connected():
    pass



@socketio.on("Log In")
def Log_In():
    print("Log In")


@socketio.on("check_user")
def check_user(data):
    room = data["phone"]
    join_room(room)
    session["user"] = data["phone"]
    session['o_id'] = data['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.check_user(data)
    if Val == 0:
        socketio.emit("check_user_status", "User exists", room=room)
    else:
        socketio.emit("check_user_status", "User does not exist", room=room)


@socketio.on("Sign_Up_Request_1")
def sign_up_request_1(data):
    room = data["phone"]
    join_room(room)
    session["user"] = data["phone"]
    session['o_id'] = data['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.signup_user(data)
    if Val == True:
        socketio.emit("sign_up_status", "Verify your number using the OTP we just shared with you.", room=room)
    else:
        socketio.emit("sign_up_status", "This number already exists in organisation.", room=room)


@socketio.on("Sign_Up_Request")
def sign_up_request(data):
    room = session['user']
    join_room(room)
    data["phone"] = session["user"]
    session['name'] = data["name"]
    # data['o_id'] = session['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.signup_user_data(data)
    if Val == True:
        socketio.emit("sign_up_status", "Redirecting ...", room=room)
    else:
        socketio.emit("sign_up_status", "This name and number already exists in organisation.", room=room)


@socketio.on("verify_otp_data")
def verify_otp_data(data):
    print("In verify otp data")
    if 'user' in session:
        phone = session['user']
        room = session['user']
        join_room(room)
        otp = database_operations.return_otp(phone)
        print(str(otp['otp']))
        print(data)
        if str(otp['otp']) == str(data):
            database_operations.update_otp_verification(phone)
            socketio.emit("otp_status", "Redirecting ...", room=room)
            print("OTP successful")
        else:
            print("OTP not succesful but user found")
            socketio.emit("otp_status", "Verification not successful.", room=room)
        leave_room(room)
    else:
        room2 = request.remote_addr
        join_room(room2)
        print("OTP not succesful")
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("verify_otp_data_partial")
def verify_otp_data_partial(data):
    print("In verify otp data")
    if 'user' in session:
        phone = session['user']
        room = session['user']
        join_room(room)
        otp = database_operations.return_otp(phone)
        print(str(otp['otp']))
        print(data)
        if str(otp['otp']) == str(data):
            database_operations.update_otp_verification_partial(phone)
            socketio.emit("otp_status", "Redirecting ...", room=room)
            print("OTP successful")
        else:
            print("OTP not succesful but user found")
            socketio.emit("otp_status", "Verification not successful.", room=room)
        leave_room(room)
    else:
        room2 = request.remote_addr
        join_room(room2)
        print("OTP not succesful")
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)


@socketio.on("resend_otp")
def resend_otp():
    if 'user' in session:
        phone = session['user']
        print("Found User")
        Val = database_operations.update_otp(phone)
        print("Update requested")
    else:
        room2 = request.remote_addr
        join_room()
        socketio.emit("otp_status", "Verification not successful.", room=room2)
        leave_room(room2)

# VATSAL
@app.route("/authenticate_external_login", methods=['GET', 'POST'])
def authenticate_external_login():
    if request.method == 'POST':
        data = request.get_json(force=True)
        print(data)  # phone | password
        Val = database_operations.verify_credentials(data)
        print(Val)
        if Val == 2:
            cred = database_operations.return_credentials(data['phone'])
            print(cred)
            to_send= {
                'login': True,
                'role': cred['role'],
                'name': cred['name'],
                'response': 2,
                'o_id': cred["o_id"],

            }
            return to_send

        if Val == 1:
            cred = database_operations.return_credentials(data['phone'])
            print(type(cred))
            to_send = {
                'login': False,
                'role': cred['role'],
                'name': cred['name'],
                'response': 1,
                'o_id': cred["o_id"]
            }

            return to_send
        if Val == 0:
            return "0"


@app.route("/authenticate_external_autologin", methods=['GET', 'POST'])
def authenticate_external_autologin():
    if request.method == 'POST':
        data = request.get_json(force=True)
        print(data)  # phone | password
        # get credentials by phone
        cred = database_operations.return_credentials(data['phone'])
        print(cred)
        to_verify = {}
        to_verify["phone"] = data["phone"]
        to_verify["password"] = cred["password"]
        Val = database_operations.verify_credentials(to_verify)
        print(Val)
        if Val == 2:
            cred = database_operations.return_credentials(data['phone'])
            print(cred)
            to_send= {
                'login': True,
                'role': cred['role'],
                'name': cred['name'],
                'response': 2,
                'o_id': cred["o_id"],
                'password': cred["password"]
            }
            return to_send

        if Val == 1:
            cred = database_operations.return_credentials(data['phone'])
            print(type(cred))
            to_send = {
                'login': False,
                'role': cred['role'],
                'name': cred['name'],
                'response': 1,
                'o_id': cred["o_id"],
                'password': cred["password"]
            }
            return to_send

        if Val == 0:
            return "0"

@app.route("/SEND_CREDENTIALS", methods=['GET', 'POST'])
def send_credentials_to_other_apps():
    if request.method == 'POST':
        data = request.get_json(force=True)
        print(data)  # phone
        # get credentials by phone
        cred = database_operations.return_credentials(data)
        print(cred)
        print("CREDENTIALS SENT!")
        return cred


@app.route("/GET_USERS_FOR_SMS", methods=['GET', 'POST'])
def get_users_for_sms():
    if request.method == 'POST':
        data = request.get_json(force=True)
        print(data)

        if data == "trace":
            users = database_operations.send_users_for_sms_trace()
            to_send = {"users": users}
            return to_send

@app.route('/external_login/<phone>')
def external_login(phone):
    # get credentials from OEE

    credentials = database_operations.return_credentials(phone)
    # {'name': True, 'password': True, 'role': True, 'o_id': True, "_id": False}
    session["user"] = phone
    session["login"] = True
    session['role'] = credentials['role']
    session['name'] = credentials['name']
    session['o_id'] = credentials['o_id']
    session['password'] = credentials['password']
    return redirect(url_for('index'))

@socketio.on("login_cred")
def login_cred(data):
    room = data['phone']
    join_room(room)
    print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        socketio.emit("login_status", "2", room=room)
        leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        socketio.emit("login_status", "1", room=room)
    if Val == 0:
        socketio.emit("login_status", "0", room=room)


@socketio.on("login_cred_check")
def login_cred_check(data):
    room = data['phone']
    join_room(room)
    print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        socketio.emit("login_status_check", "2", room=room)
        leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        socketio.emit("login_status_check", "1", room=room)
    if Val == 0:
        socketio.emit("login_status_check", "0", room=room)


@app.route('/autologin', methods=['GET'])
def autologin():
    data = {}
    data['phone'] = request.args.get('phone')
    data['password'] = request.args.get('password')

    print("----------------------------")
    print(data)
    print("----------------------------")
    # room = data["phone"]
    # join_room(room)
    # print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        # socketio.emit("login_status","2",room=room)

        # -------------------------------------------------------------------------

        if 'user' in session and 'login' in session:
            if session['login']:
                return redirect(url_for('epass'))
                '''color="info"
                phone = session['user']
                epass = database_operations.get_epass(phone)
                #print(epass)
                timestamp = ""
                button_text = ""
                if "timestamp" in  epass :
                    timestamp ="Last record on : "+  epass['timestamp'].strftime("%d/%m/%Y %H:%M:%S")
                    if (datetime.datetime.now() + datetime.timedelta(minutes=330) - epass['timestamp'] ) > datetime.timedelta(1):
                        #print("1 day elapsed")
                        epass['epass_status']="No Application found in last 24 hours."
                        epass['qrcode_link'] = '/epass/fillup.jpg'
                        #print(timestamp)
                        color = "warning"
                        button_text = "Apply for E-Pass"
                    if epass['epass_status'] == 'Accepted' :
                        color = "success"
                        button_text = "Re-apply for E-Pass"
                    if epass['epass_status'] == 'Rejected':
                        color = "danger"
                        button_text = "Retry for E-Pass"
                    #print(color)
                else :
                    epass['epass_status'] = "You do not have any E-Pass Applications. Please answer questions in Self-Declaration section to get an E-Pass"
                    epass['qrcode_link'] = '/epass/fillup.jpg'
                    button_text = "Go to Self-Declaration "
					
			
				
				
                return render_template("epass.html",status=epass['epass_status'],
                                       last_app=timestamp,
                                       src=epass["qrcode_link"],
                                       color=color,
                                       active_page ='epass',
                                       role=session['role'],
                                       name=session['name'],
                                       button_text = button_text
                                       )'''


        else:
            return redirect(url_for('login'))

        # ----------------------------------------------------------------------------------------------------------------------

        # leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        # session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['role'] = data1['role']
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        # render_template('sign.html')
        return redirect(url_for('login'))
        ###return render_template("signup.html")
        # socketio.emit("login_status", "1",room=room)
    if Val == 0:
        # socketio.emit("login_status", "0",room=room)
        return redirect(url_for('login'))


@socketio.on("forgot_generate_otp")
def forgot_generate_otp(phone):
    room = phone
    join_room(phone)
    Val = database_operations.update_otp(phone)
    print("Update requested")
    leave_room(phone)
    # Send otp


@socketio.on("forgot_verify_otp")
def forgot_verify_otp(data):
    room = data["phone"]
    join_room(room)
    otp = database_operations.return_otp(data['phone'])
    if str(otp['otp']) == str(data['otp']):
        socketio.emit("otp_status", "Redirecting ...", room=room)
        print("OTP successful")
        session['user'] = data['phone']
        leave_room(room)
    else:
        print("OTP not succesful but user found")
        socketio.emit("otp_status", "Verification not successful.", room=room)


@socketio.on("reset_password")
def forgot_verify_otp(password):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = {}
        data['phone'] = session['user']
        data['password'] = password
        print("Found User")
        Val = database_operations.reset_password(data)
        if Val == True:
            socketio.emit("update_pass_status", "True", room=room)
            leave_room(room)
        else:
            socketio.emit("update_pass_status", "False", room=room)


@socketio.on("logout")
def logout():
    if 'user' in session:
        session.pop('user')
        session.pop("role")
        session.pop("name")
        session.pop('o_id')
        session.pop('login')
        print("Logged Out")
    else:
        print("User not found !")


@socketio.on("declaration_data")
def declaration(data):
    print(data)
    if 'user' in session:
        phone = session['user']
        room = phone
        join_room(room)
        Val = database_operations.update_declaration(phone, data, session)
        time.sleep(1)
        socketio.emit("declaration_submitted", room=room)
        # leave_room(room)


@app.route('/template')
def template():
    return render_template("template.html")


# ===================================================

@app.route('/epass/<path:filename>')
def download_file(filename):
    print('correct loop')
    return send_from_directory('code', filename, as_attachment=True)




@app.route('/')
@app.route('/login')
def login():
    if 'user' in session:
        phone = session['user']
        credentials = database_operations.return_credentials(phone)
        return render_template("signin.html", phone=phone, password=credentials['password'],
                               role=credentials['role'], o_id=credentials['o_id'])
    else:
        return render_template("signin.html")

# vatsal
#--------------------------- LOGIN FOR EMAIL REPORTS-------------------------------------------------------------------
@app.route('/SPR_view/<date>/<shift>/<machine>')
def email_login(date,shift,machine):

    print("i am here")
    if 'user' in session:
        phone = session['user']
        credentials = database_operations.return_credentials(phone)
        print("login found")
        # USER ALREADY LOGGED IN

        return render_template("email_signin.html", phone=phone, password=credentials['password'],
                               role=credentials['role'], o_id=credentials['o_id'],date = date, shift = shift,
                               machine= machine)

    else:
        print("login not found")

        return render_template("email_signin.html",date = date, shift = shift, machine= machine)

# ----------------------------------------------------------------------------------------------------------------------



@app.route('/createaccount')
def createaccount():
    return render_template("createaccount.html")


@app.route('/signup')
def signup():
    questions = database_operations.get_signup_questions()
    return render_template("signup.html")


@app.route('/signup1')
def signup1():
    return render_template("signup1.html")


@app.route('/signup2')
def signup2():
    return render_template("signup2.html")


@app.route('/verifyotp')
def verifyotp():
    if 'user' in session:
        print(session['user'])
        return render_template("verifyotp.html", user=session["user"])
    else:
        return render_template("signup.html")


@app.route('/forgotpassword')
def forgotpassword():
    return render_template("forgotpassword.html")


@app.route('/newpassword')
def newpassword():
    if 'user' in session:
        return render_template("newpassword.html")
    else:
        return render_template("signup.html")


@app.route('/contact')
def contact():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("contact.html",
                                   active_page='contact',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/admin')
def admin():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("admin.html",
                                   active_page='admin',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/applications')
def application():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("application.html",
                                   active_page='applications',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("application_data")
def application_data():
    if 'user' in session:
        room = session['user']
        join_room(room)

        data = database_operations.application_data(session['o_id'])
        socketio.emit("application", data, room=room)


@socketio.on("change_status")
def change_status(arr):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.change_status(arr, session['o_id'])
        # socketio.emit("application",data,room=room)


@socketio.on("question")
def cquestion(question):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.log_question(question, session)
        # socketio.emit("application",data,room=room)


@socketio.on("modal_application")
def modal_application(x):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.modal_application(x, session['o_id'])
        socketio.emit("modal_data_application", data, room=room)


@socketio.on("updated_role")
def updated_role(arr):
    if 'user' in session:
        room = session["user"]
        data = database_operations.update_role(arr, session['o_id'])
        return "ok"


@socketio.on("updated_employee_status")
def updated_employee_status(arr):
    print(arr)
    if 'user' in session:
        room = session["user"]
        data = database_operations.update_employee_type(arr, session['o_id'])
        return "ok"

@app.route('/masterlogin')
def masterlogin():
    return render_template("master-signin.html")


@socketio.on("master_login_credentials")
def master_login_credentials(data):
    room = request.sid
    join_room(room)
    print("Master Login Credentials received")
    if data["phone"] == "Admin" and data["password"] == "Anzen2020":
        socketio.emit("master_credentials_validation", "1", room=room)
        session["master_user"] = True
    else:
        socketio.emit("master_credentials_validation", "0", room=room)
        session["master_user"] = False


# ----------------- MAster Application -------------
@app.route('/master-applications')
def masterapplications():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-application.html",
                                   active_page='master-applications',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@socketio.on("master_application_data")
def master_application_data():
    if 'master_user' in session:
        room = request.sid
        join_room(room)

        data = database_operations.master_application_data()
        socketio.emit("master_application", data, room=room)


@socketio.on("master_change_status")
def master_change_status(arr):
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.master_change_status(arr)
        socketio.emit("master_application", data, room=room)


@socketio.on("master_modal_application")
def master_modal_application(x):
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.master_modal_application(x)
        socketio.emit("master_modal_data_application", data, room=room)


@socketio.on("master_updated_role")
def master_updated_role(arr):
    if 'master_user' in session:
        room = request.sid
        print("Requested Role Update")
        data = database_operations.master_update_role(arr)
        return "ok"


@socketio.on("master_updated_employee_status")
def updated_employee_status(arr):
    print(arr)
    if 'master_user' in session:
        room = request.sid
        print("Reqested Employee Status Updated ")
        data = database_operations.master_update_employee_type(arr)
        return "ok"


@socketio.on("master_logout")
def master_logout():
    if 'master_user' in session:
        session.pop('master_user')
        print("Master Logged Out")
    else:
        print("User not found !")


@socketio.on("save_question_changes")
def save_question_changes(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.save_questions(data)
        socketio.emit("master_console", "Saved !", room=room)
    else:
        print("User not found !")


@socketio.on("customer_onboarding")
def customer_onboarding(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.on_board_customer(data)
        if data == 0:
            socketio.emit("master_console", "Customer with Same Organisation ID exists !", room=room)
        else:
            socketio.emit("master_console", "Congratulations ! New Customer On-boarded .", room=room)
    else:
        print("User not found !")


@app.route('/master-onboarding')
def master_onboarding():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-onboarding.html",
                                   active_page='master-onboarding',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@app.route('/master-questions')
def master_questions():
    if 'master_user' in session:
        if session['master_user']:
            return render_template("master-questions.html",
                                   active_page='master-questions',
                                   )
    else:
        return redirect(url_for('masterlogin'))


@socketio.on("get_master_questions")
def get_master_questions():
    if 'master_user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.get_questions()
        print("--------------------------")
        print(data)
        print("--------------------------")

        socketio.emit("sent_master_questions", data, room=room)


@app.route('/all_questions')
def all_questions():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template(".html",
                                   active_page='settings',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


# -----------------------------------------------------

@app.route('/settings')
def settings():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("settings.html",
                                   active_page='settings',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("settings_info")
def get_settings_info():
    if 'user' in session:
        room = session["user"]
        room = request.sid
        join_room(room)
        val, data = database_operations.get_settings(session['o_id'])
        print(data)
        socketio.emit("settings_info_return", data)
        leave_room(room)
        return "ok"


@socketio.on("change_settings")
def change_settings(data):
    if 'user' in session:
        room = session["user"]
        room = request.sid
        join_room(room)
        database_operations.change_settings(session['o_id'], data)
        socketio.emit("settings_saved")
        leave_room(room)
        return "ok"


@socketio.on("get_signup_declaration_questions")
def get_signup_declaration_questions():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        join_room(room)
        data = database_operations.get_signup_questions(o_id)
        print("--------------------------")
        print(data)
        print("--------------------------")
        socketio.emit("sent_signup_declaration_questions", data, room=room)


@socketio.on("declaration_data_new")
def declaration_new(data):
    print("--------------------------")
    print(data)
    print("--------------------------")

    if 'user' in session:
        phone = session['user']
        room = phone
        join_room(room)
        Val = database_operations.update_declaration_new(phone, data, session)
        time.sleep(1)
        socketio.emit("declaration_submitted", room=room)
        # leave_room(room)


@socketio.on("organisation_settings_change")
def organisation_settings_change(data):
    if 'master_user' in session:
        room = request.sid
        data, val = database_operations.organisation_settings_change(data, session['o_id'])
        if data == 0:
            socketio.emit("master_console", "Settings not saved.", room=room)
        else:
            socketio.emit("master_console", "Settings saved successfully !", room=room)
    else:
        print("User not found !")


@socketio.on("get_settings_oid")
def get_settings_oid():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.get_settings_oid(o_id)
        if data == []:
            pass
        else:
            socketio.emit("send_settings_oid", data, room=room)

# ------------------------------------

@app.route('/profile')
def profile():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("profile.html",
                                   active_page='profile',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_profile")
def get_profile():
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.get_profile(session['user'])
        if data == []:
            pass
        else:
            socketio.emit("send_profile", data, room=room)


@socketio.on("Profile_Update")
def profile_update(dataset):
    if 'user' in session:
        o_id = session['o_id']
        room = request.sid
        data = database_operations.update_profile(dataset, session['user'])
        socketio.emit("master_console", "Data saved successfully !", room=room)


@app.route('/new_notifications', methods=['POST', 'GET'])
def new_notifications():
    data = {}
    session = {}
    session['name'] = "Android App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        print("------ Notification Request Recieved -----")
        print(data)
        print("-------------------------------------------")
        return jsonify({"response": "Notification Response"})


@app.route('/message_compose')
def message_compose():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("message_compose.html",
                                   active_page='notifications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("message_compose_data")
def message_compose_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.message_compose_data(session['o_id'])
        socketio.emit("message_application", data, room=room)


@socketio.on("composed_message")
def composed_message(data):
    if 'user' in session:
        room = session['user']
        join_room(room)
        # data = database_operations.composed_message(session['o_id'])
        print("-------------------COMPOSED MESSAGE -------------")
        print(data)
        print("----------------------------------------")

        data = database_operations.send_message(data, session)

        # socketio.emit("message_application", data, room=room)


# -------------------------- Register USer TOKEN from Device ------------------------
@app.route('/register_token', methods=['POST', 'GET'])
def register_token():
    data = {}
    session = {}
    session['name'] = "App"
    if request.method == 'POST':
        print(request.json)
        data = request.json
        print("------ Register TOKEN Request Recieved -----")
        print(data)
        val = database_operations.add_user_registration_token(data)
        print("-------------------------------------------")
        return jsonify({"response": "RECEIVED"})


# ----------------------
# Question Settings
# ----------------------


@app.route('/questions')
def oid_questions():
    if 'user' in session:
        if session['user']:
            return render_template("question_settings.html",
                                   active_page='questions',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_oid_questions")
def get_oid_questions():
    if 'user' in session:
        room = request.sid
        join_room(room)
        data = database_operations.get_questions_oid(session['o_id'])
        # print("--------------------------")
        # print(data)
        # print("--------------------------")
        to_send = data[0]["question_settings"]
        print("--------------------------")
        print(to_send)
        print("--------------------------")

        socketio.emit("sent_oid_questions", to_send, room=room)


@socketio.on("save_question_changes_oid")
def save_question_changes_oid(data):
    if 'user' in session:
        room = request.sid
        data, val = database_operations.save_questions_oid(data, session["o_id"])
        socketio.emit("master_console", "Saved !", room=room)
    else:
        print("User not found !")


# ------- MESSAGE HISTORY ----------

@app.route('/notifications')
def notifications():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("all_notifications.html",
                                   active_page='notifications',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_all_messages")
def get_all_messages():
    if 'user' in session:
        room = request.sid
        val, data = database_operations.get_all_messages(session)
        print(" ------------------------ Get All Messages -----------------")
        if val == 0:
            socketio.emit("master_console", "No messages found !", room=room)
        if val == 1:
            socketio.emit("send_all_messages", data, room=room)


@socketio.on("attendance_data_csv")
def attendance_data_csv(data):
    if 'user' in session:
        room = request.sid
        # print(data)
        import numpy as np
        m, n = np.shape(data)
        # field names
        # print(m,n)
        print("Processing Data")
        for i in range(0, m):
            if str(data[i][10]).__contains__("UNFREEZE"):
                data[i][9] = "FROZEN"
            else:
                data[i][9] = " - "
            data[i][10] = ""
        print("Process complete")

        fields = ['Name', 'Number', 'Date', 'Timestamp', 'Employee Type', 'Employee ID', 'E Pass Status', 'Temperature',
                  'Attendance Status', 'Account Status']

        filename = "Documents/" + str(uuid.uuid1()) + ".csv"
        # writing to csv file
        with open(filename, 'w') as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile)

            # writing the fields
            csvwriter.writerow(fields)
            # writing the data rows
            csvwriter.writerows(data)

        socketio.emit("filename", filename, room=room)


@socketio.on("application_data_csv")
def application_data_csv():
    if 'user' in session:
        room = request.sid
        data = database_operations.application_data_csv(session["o_id"])
        # print(data[0])

        fields = ['Name', 'Number', 'Date', "Timestamp", 'Employee Type', 'Employee ID', 'Contract Organisation',
                  'Application Status']

        filename = "Documents/" + str(uuid.uuid1()) + ".csv"
        # writing to csv file
        with open(filename, 'w') as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile)

            # writing the fields
            csvwriter.writerow(fields)
            # writing the data rows
            csvwriter.writerows(data)

        socketio.emit("filename", filename, room=room)


@app.route('/Documents/<path:filename>')
def download_document(filename):
    print('correct loop')
    return send_from_directory('Documents', filename, as_attachment=True)


@socketio.on("freeze_account")
def freeze_account(phone):
    if 'user' in session:
        room = request.sid
        data = database_operations.freeze_account(phone)
        print(" ------------------------ Freeze Account -----------------")


@socketio.on("unfreeze_account")
def unfreeze_account(phone):
    if 'user' in session:
        room = request.sid
        data = database_operations.unfreeze_account(phone)
        print(" ------------------------ Freeze Account -----------------")





'''====================================================================='''
'''====================================================================='''
'''===================        OEE       ================================'''
'''====================================================================='''
'''====================================================================='''


@app.route('/file_upload', methods=['GET', 'POST'])
def file_upload():
    print("file loading")
    f = 0
    if request.method == 'POST':
        print("file received")
        f = request.files['file']

        f.save((planning_sheet_save_path + f.filename))
        print("file=", f.filename)
        return jsonify({'file_upload_successful': f.filename})

    return render_template("production_plan.html",
                           active_page='production_plan',
                           role=session['role'],
                           name=session['name'],
                           file_status="Uploaded Successfully!",
                           filename=f.filename
                           )


@socketio.on("sheet_name_oee")
def sheet_name_oee(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data["sheet_name"])
        print(data["file_name"])
        # data,error_quantity,error_partnumber,error_machine = read_planning_sheet.parse_excel("POS.xlsx", "20" )
        status, data, errors, errors_part_number, errors_machine, used_orders = read_planning_sheet.read_excel(
            planning_sheet_save_path + data["file_name"], data["sheet_name"])
        socketio.emit("uploaded_sheet_status", status, room=room)

        time.sleep(1)
        to_send = [
            str(len(data)),
            str(len(errors)),
            str(len(errors_part_number)),
            str(len(errors_machine)),
            str(len(used_orders)),
        ]
        print(to_send)
        socketio.emit("update_bar_graph", to_send, room=room)

        to_send = [
            str(len(data + errors + errors_part_number + errors_machine)),
            str(len(errors + errors_part_number + errors_machine)),
            str(len(data)),

        ]
        print(to_send)
        socketio.emit("update_round_graph", to_send, room=room)

        # print(data[0])
        new_data = []
        # STRF DATETIME
        for record in used_orders:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass

        if len(new_data) != 0:
            socketio.emit("uploaded_used_data_oee", new_data, room=room)
            time.sleep(1)

        # print(data[0])
        new_data = []

        print(" ================== VALID ORDERS ====================")
        print(len(data))
        print(" =====================================================")

        # STRF DATETIME
        print()
        for record in data:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                new_data.append(record)
                '''print(" ================== NEW VALID ORDERS ====================")
                print(json.dumps(record, indent=4))
                print(" =====================================================")'''

        if len(new_data) != 0:
            socketio.emit("uploaded_data_oee", new_data, room=room)
            time.sleep(1)

        new_data = []
        # STRF DATETIME
        for record in errors:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass
        if len(new_data) != 0:
            socketio.emit("error_quantity_oee", new_data, room=room)
            time.sleep(1)

        time.sleep(1)
        new_data = []
        # STRF DATETIME
        for record in errors_part_number:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass
        if len(new_data) != 0:
            socketio.emit("error_partnumber_oee", new_data, room=room)
            time.sleep(1)

        new_data = []
        # STRF DATETIME
        for record in errors_machine:
            try:
                record["DATE"] = record["DATE"].strftime("%d-%b-%Y")
                record["TIME"] = record["TIME"].strftime("%H:%M:%S")
                new_data.append(record)
            except:
                pass
        if len(new_data) != 0:
            socketio.emit("error_machine_oee", new_data, room=room)
            time.sleep(1)


@socketio.on("proceed_to_production_plan_2")
def proceed_to_production_plan_2(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("===============IN TEMP PRODUCTION PLAN=============")
        print(len(data))
        new_database_operations.temp_insert_production_orders(data, session)

        print(" PRODCUTION ORDERS INSERTED in TEMP DATA")


@socketio.on("proceed_to_production_plan_3")
def proceed_to_production_plan_3(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("===============IN TEMP PRODUCTION PLAN 2 =============")
        print(len(data))
        new_database_operations.temp_insert_production_orders(data, session)

        print(" PRODCUTION ORDERS INSERTED in TEMP DATA")


@socketio.on("save_production_plan")
def save_production_plan(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============== SAVING PRODUCTION PLAN =============")
        print(len(data))
        # convert time string of datetime to time format
        # ADD stop time planned
        for record in data:
            record["PLANNED_START"] = datetime.datetime.strptime(record["DATE"] + " :: " + record["TIME"],
                                                                 '%d-%b-%Y :: %H:%M:%S')
            print(record["CYCLE TIME"])
            print(record["NEW QTY"])
            record["PLANNED_STOP"] = record["PLANNED_START"] + datetime.timedelta(
                seconds=(float(record["CYCLE TIME"]) * float(record["NEW QTY"])))
            record["ACTUAL_START"] = "-"
            record["ACTUAL_STOP"] = "-"
            record["STATUS"] = "Planned"
            print(record["PLANNED_START"])

        new_database_operations.insert_production_orders(data)
        new_database_operations.clear_temp_production_orders(session)

        print(" PRODCUTION ORDERS INSERTED in TEMP DATA")


@socketio.on("get_temp_production_orders")
def get_temp_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============SEND PRODUCTION ORDERS TEMP================")
        production_orders = new_database_operations.temp_get_production_orders(session)
        for production_order in production_orders:
            backend_params = new_database_operations.query_backend_database(production_order["PART NUMBER"])

            if bool(backend_params):

                if "DA_TAPE" in backend_params:
                    production_order["DA_TAPE"] = backend_params["DA_TAPE"]
                else:
                    production_order["DA_TAPE"] = "Not Found"

                if "TAB_TAPE" in backend_params:
                    production_order["TAB_TAPE"] = backend_params["TAB_TAPE"]
                else:
                    production_order["TAB_TAPE"] = "Not Found"

                if "child_parts" in backend_params:
                    production_order["child_parts"] = backend_params["child_parts"]
                else:
                    production_order["child_parts"] = ["Not Found"]

                # check if it is Primary or Secondary Machine
                print(json.dumps(backend_params, indent=4))
                if "PRIMARY MACHINE" in backend_params:
                    print("=======================================")
                    print(production_order["MACHINE"])
                    print(backend_params["PRIMARY MACHINE"])
                    print(backend_params["SECONDARY MACHINE"])

                    if production_order["MACHINE"] == backend_params["PRIMARY MACHINE"]:
                        print("Matched PM ")
                        production_order["MACHINE TYPE"] = "Primary"
                        production_order["CYCLE TIME"] = backend_params["PM CYCLE TIME(SEC)"]
                        if production_order["CYCLE TIME"] != "":
                            production_order["MACHINE STATUS"] = "Valid"
                        else:
                            production_order["MACHINE STATUS"] = "Invalid"
                        production_order["PRD/HR"] = backend_params["PM PRD/HR"]
                        production_order["PRD/SHIFT"] = backend_params["PM PRD/SHIFT"]
                        production_order["LINE REJECTION"] = backend_params["PM LINE REJECTION"]
                        production_order["START UP TIME (MIN)"] = backend_params["PM START UP TIME (MIN)"]
                        production_order["START UP SCRAP(KG)"] = backend_params["PM START UP SCRAP(KG) "]
                        production_order["MOULD CHANGE TIME (MIN)"] = backend_params["PM MOULD CHANGE TIME (MIN) "]

                    elif production_order["MACHINE"] == backend_params["SECONDARY MACHINE"]:
                        print("Matched SM ")
                        production_order["MACHINE TYPE"] = "Secondary"
                        production_order["CYCLE TIME"] = backend_params["SM CYCLE TIME(SEC)"]
                        if production_order["CYCLE TIME"] != "":
                            production_order["MACHINE STATUS"] = "Valid"
                        else:
                            production_order["MACHINE STATUS"] = "Invalid"
                        production_order["PRD/HR"] = backend_params["SM PRD/HR"]
                        production_order["PRD/SHIFT"] = backend_params["SM PRD/SHIFT"]
                        production_order["LINE REJECTION"] = backend_params["SM LINE REJECTION"]
                        production_order["START UP TIME (MIN)"] = backend_params["SM START UP TIME (MIN)"]
                        production_order["START UP SCRAP(KG)"] = backend_params["SM START UP SCRAP(KG) "]
                        production_order["MOULD CHANGE TIME (MIN)"] = backend_params["SM MOULD CHANGE TIME (MIN) "]

                    else:
                        print("No Match ")
                        production_order["MACHINE TYPE"] = " -- "
                        production_order["CYCLE TIME"] = ""
                        if production_order["CYCLE TIME"] != "":
                            production_order["MACHINE STATUS"] = "Valid"
                        else:
                            production_order["MACHINE STATUS"] = "Invalid"
                        production_order["PRD/HR"] = " -- "
                        production_order["PRD/SHIFT"] = " -- "
                        production_order["LINE REJECTION"] = " -- "
                        production_order["START UP TIME (MIN)"] = " -- "
                        production_order["START UP SCRAP(KG)"] = " -- "
                        production_order["MOULD CHANGE TIME (MIN)"] = " -- "

                    print("=======================================")

            else:
                production_order["DA_TAPE"] = "Not in Database"
                production_order["TAB_TAPE"] = "Not in Database"
                production_order["child_parts"] = ["Not in Database"]

                production_order["MACHINE TYPE"] = " -- "
                production_order["CYCLE TIME"] = ""
                if production_order["CYCLE TIME"] != "":
                    production_order["MACHINE STATUS"] = "Valid"
                else:
                    production_order["MACHINE STATUS"] = "Invalid"
                production_order["PRD/HR"] = " -- "
                production_order["PRD/SHIFT"] = " -- "
                production_order["LINE REJECTION"] = " -- "
                production_order["START UP TIME (MIN)"] = " -- "
                production_order["START UP SCRAP(KG)"] = " -- "
                production_order["MOULD CHANGE TIME (MIN)"] = " -- "

        socketio.emit("sent_temp_production_orders", production_orders, room=room)
        print(" TEMP PRODCUTION ORDERS SENT ")


@socketio.on("get_production_orders")
def get_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        production_orders = new_database_operations.get_production_orders()

        # Seriallize Planned Start Time and End Time

        for record in production_orders:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["PLANNED_START"]:
                    record["STATUS"] = "Planned"
                if record["PLANNED_START"] <= current_time <= record["PLANNED_STOP"]:
                    record["STATUS"] = "Live"
                if current_time > record["PLANNED_STOP"]:
                    record["STATUS"] = "Done"
                if "ACTION" in record:
                    pass
                else:
                    record["ACTION"] = "Start"
                new_database_operations.update_specific_production_order(record)

                record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
                record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
                try:
                    record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                    record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
                except:
                    pass

            except:
                pass
        socketio.emit("sent_production_orders", production_orders, room=room)
        print(" PRODCUTION ORDERS SENT ")


@socketio.on("get_active_production_orders")
def get_active_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        #print("==============active prod orders  =============")
        production_orders = new_database_operations.get_active_production_orders()

        # Seriallize Planned Start Time and End Time

        for record in production_orders:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["PLANNED_START"]:
                    record["STATUS"] = "Planned"
                if record["PLANNED_START"] <= current_time <= record["PLANNED_STOP"]:
                    record["STATUS"] = "Live"
                if current_time > record["PLANNED_STOP"]:
                    record["STATUS"] = "Done"
                if "ACTION" in record:
                    pass
                else:
                    record["ACTION"] = "Start"
                new_database_operations.update_specific_production_order(record)


                try:
                    record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
                    record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
                    record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                    record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
                except:
                    pass

            except:
                pass
        socketio.emit("sent_active_production_orders", production_orders, room=room)
        print("ACTIVE PRODCUTION ORDERS SENT ")


@socketio.on("get_done_production_orders")
def get_done_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        production_orders = new_database_operations.get_done_production_orders()

        # Seriallize Planned Start Time and End Time

        for record in production_orders:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["PLANNED_START"]:
                    record["STATUS"] = "Planned"
                if record["PLANNED_START"] <= current_time <= record["PLANNED_STOP"]:
                    record["STATUS"] = "Live"
                if current_time > record["PLANNED_STOP"]:
                    record["STATUS"] = "Done"
                if "ACTION" in record:
                    pass
                else:
                    record["ACTION"] = "Start"
                new_database_operations.update_specific_production_order(record)

                record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
                record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
                try:
                    record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                    record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
                except:
                    pass

            except:
                pass
        socketio.emit("sent_done_production_orders", production_orders, room=room)
        print("DONE PRODCUTION ORDERS SENT ")


@socketio.on("get_planned_production_orders")
def get_planned_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        production_orders = new_database_operations.get_planned_production_orders()

        # Seriallize Planned Start Time and End Time

        for record in production_orders:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["PLANNED_START"]:
                    record["STATUS"] = "Planned"
                if record["PLANNED_START"] <= current_time <= record["PLANNED_STOP"]:
                    record["STATUS"] = "Live"
                if current_time > record["PLANNED_STOP"]:
                    record["STATUS"] = "Done"
                if "ACTION" in record:
                    pass
                else:
                    record["ACTION"] = "Start"
                new_database_operations.update_specific_production_order(record)

                record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
                record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
                try:
                    record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                    record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
                except:
                    pass

            except:
                pass
        socketio.emit("sent_planned_production_orders", production_orders, room=room)
        print("PLANNED PRODCUTION ORDERS SENT ")


#POST RESPONSE
@app.route("/get_planned_production_orders1")
def get_planned_production_orders1():
    print("============================")
    data = new_database_operations.get_planned_production_orders()
    for i in range(len(data)):
        try:
            current_time = datetime.datetime.now()
            if current_time < data[i]["PLANNED_START"]:
                data[i]["STATUS"] = "Planned"
            if data[i]["PLANNED_START"] <= current_time <= data[i]["PLANNED_STOP"]:
                data[i]["STATUS"] = "Live"
            if current_time > data[i]["PLANNED_STOP"]:
                data[i]["STATUS"] = "Done"
            if "ACTION" in data:
                pass
            else:
                data[i]["ACTION"] = "Start"
            new_database_operations.update_specific_production_order(data[i])

            data[i]["PLANNED_START"] = data[i]["PLANNED_START"].strftime(constants.time_format)
            data[i]["PLANNED_STOP"] = data[i]["PLANNED_STOP"].strftime(constants.time_format)
            try:
                data[i]["ACTUAL_START"] = data[i]["ACTUAL_START"].strftime(constants.time_format)
                data[i]["ACTUAL_STOP"] = data[i]["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass

            if (data[i]["ACTION"] == "Start") :
                data[i]['Item']= str(i+1)
                if (data[i]['STATUS'] == "Planned" ) :
                    data[i]['BSTATUS']= '<div class="btn btn-sm Rectangle_without_color btn-primary">'+data[i]['STATUS']+'</div>';

                elif (data[i]['STATUS'] == "Live" ):
                    data[i]['BSTATUS']= '<div class="btn btn-sm Rectangle_without_color btn-teal">'+data[i]['STATUS']+'</div>';

                else:
                    data[i]['BSTATUS']= '<div class="btn btn-sm Rectangle_without_color btn-dark">'+data[i]['STATUS']+'</div>';



                if (data[i]['ACTION'] == "Start" ) :
                    data[i]['BACTION'] = '<div class="btn btn-sm Rectangle_without_color btn-primary" id="'+data[i]["ORDER NUMBER (LEFT)"]+'&'+data[i]["ORDER NUMBER (RIGHT)"] +'" onclick="open_modal(this)">'+data[i]['ACTION']+'</div>';


                elif (data[i]['ACTION'] == "Done" ):
                    data[i]['BACTION'] = '<div class="btn btn-sm Rectangle_without_color btn-dark id="'+data[i]["ORDER NUMBER (LEFT)"]+'&'+data[i]["ORDER NUMBER (RIGHT)"]+'" onclick="open_modal(this)">'+data[i]['ACTION']+'</div>';


                elif (data[i]['ACTION'] == "In Process" ):
                    data[i]['BACTION'] = '<div class="btn btn-sm Rectangle_without_color btn-teal" id="'+data[i]["ORDER NUMBER (LEFT)"]+'&'+data[i]["ORDER NUMBER (RIGHT)"] +'" onclick="open_modal(this)">'+data[i]['ACTION']+'</div>';



        except:
            pass
    new_data = {"data": data}

    # pprint(data[0])
    return jsonify(new_data)


@socketio.on("get_latest_production_orders")
def get_latest_production_orders():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        production_orders = new_database_operations.get_latest_production_orders()

        # Seriallize Planned Start Time and End Time

        for record in production_orders:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["PLANNED_START"]:
                    record["STATUS"] = "Planned"
                if record["PLANNED_START"] <= current_time <= record["PLANNED_STOP"]:
                    record["STATUS"] = "Live"
                if current_time > record["PLANNED_STOP"]:
                    record["STATUS"] = "Done"
                if "ACTION" in record:
                    pass
                else:
                    record["ACTION"] = "Start"
                new_database_operations.update_specific_production_order(record)

                record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
                record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
                try:
                    record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                    record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
                except:
                    pass

            except:
                pass
        socketio.emit("sent_production_orders", production_orders, room=room)
        print(" PRODCUTION ORDERS SENT ")


@socketio.on("update_specific_production_order")
def update_specific_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        new_database_operations.update_specific_production_order(data)
        # socketio.emit("sent_production_orders", production_orders , room=room)
        # Seriallize Planned Start Time and End Time
        print("UPDATED PRODUCTION ORDERS")


@socketio.on("get_specific_production_order")
def get_specific_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        record = new_database_operations.get_specific_production_order(data)
        record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
        record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
        try:
            record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
            record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
        except:
            pass

        socketio.emit("sent_specific_production_order", record, room=room)
        # Seriallize Planned Start Time and End Time
        print("SENT SPECIFIC PRODUCTION ORDERS")


@socketio.on("start_production_order")
def start_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        # Update Status to In Process
        # Update Actual Start Time
        # Add Production Order Numbers to current shift
        actual_data = copy.deepcopy(data)
        print(actual_data)
        found_paused = new_database_operations.check_if_production_order_is_paused(data)
        print("FOUND PAUSED :",  found_paused)
        if found_paused == 0:
            data["ACTUAL_START"] = datetime.datetime.now() + datetime.timedelta(constants.timezone_offset)
            data["ACTION"] = "In Process"
            new_database_operations.update_specific_production_order(data)
        else :
            print("PAUSED ORDER WAS FOUND")
            new_database_operations.resume_production_order(data)


        to_add = {
            "ORDER NUMBER (LEFT)": actual_data["ORDER NUMBER (LEFT)"],
            "ORDER NUMBER (RIGHT)": actual_data["ORDER NUMBER (RIGHT)"]
        }

        new_database_operations.update_shift_production_orders(to_add)

        print("STARTED PRODUCTION ORDER")
        socketio.emit("console", "Production order Started", room=room)
        if not constants.test_environment:
            send_active_thread = threading.Thread(target=rest_api.send_active_production_orders())
            send_active_thread.start()

            send_planned_thread = threading.Thread(target=rest_api.send_planned_production_orders())
            send_planned_thread.start()


@socketio.on("pause_production_order")
def pause_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        # Update Status to In Process
        # Update Actual Start Time
        # Add Production Order Numbers to current shift
        actual_data = copy.deepcopy(data)
        print(actual_data)

        #data["ACTUAL_START"] = datetime.datetime.now() + datetime.timedelta(constants.timezone_offset)
        # data["ACTION"] = "Paused"
        # new_database_operations.update_specific_production_order(data)
        new_database_operations.pause_production_order(data)
        to_add = {
            "ORDER NUMBER (LEFT)": actual_data["ORDER NUMBER (LEFT)"],
            "ORDER NUMBER (RIGHT)": actual_data["ORDER NUMBER (RIGHT)"]
        }

        new_database_operations.update_shift_production_orders(to_add)


        # socketio.emit("console", "Production order Started", room=room)
        # ================================================================
        # -------------SEND ACTIVE ORDERS TO CBM-----------------VATSAL
        active_orders = new_database_operations.get_active_production_orders()

        for record in active_orders:
            try:
                record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
                record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass
        if not constants.test_environment:

            send_active_thread = threading.Thread(target=rest_api.send_active_production_orders())
            send_active_thread.start()

            send_planned_thread = threading.Thread(target=rest_api.send_planned_production_orders())
            send_planned_thread.start()

        # ================================================================
        # -------------SEND PLANNED ORDERS TO TRACE----------------- VATSAL
        # =================================================================




@socketio.on("stop_production_order")
def stop_production_order(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        # Update Status to In Process
        # Update Actual Start Time
        # Add Production Order Numbers to current shift
        actual_data = copy.deepcopy(data)
        print(actual_data)

        data["ACTUAL_STOP"] = datetime.datetime.now() + datetime.timedelta(constants.timezone_offset)
        data["ACTION"] = "Done"
        new_database_operations.update_specific_production_order(data)

        # new_database_operations.update_shift_production_orders([actual_data["ORDER NUMBER (LEFT)"],actual_data["ORDER NUMBER (LEFT)"]])

        print("STOPPED PRODUCTION ORDER")
        socketio.emit("console", "Production order Stopped", room=room)
        if not constants.test_environment:

            send_active_thread = threading.Thread(target=rest_api.send_active_production_orders())
            send_active_thread.start()

            send_planned_thread = threading.Thread(target=rest_api.send_planned_production_orders())
            send_planned_thread.start()


@socketio.on("get_production_order_details_by_machine")
def get_production_order_details_by_machine(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        #print("============================")
        # QUERY FORMAT
        record, found = new_database_operations.get_production_order_details_by_machine(data)
        if found:
            #print("FOUND IN_PROCESS PRODUCTION ORDER")
            socketio.emit("console", "Production Order In Process", room=room)
            time.sleep(0.5)
            record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass
            #print("----------------")
            #print(record)
            #print("-----------------")
            socketio.emit("send_production_order", record, room=room)
        else:
            print("PRODUCTION ORDER NOT FOUND !")
            socketio.emit("send_production_order", {}, room=room)
            socketio.emit("console", "No Production Order", room=room)
            time.sleep(0.5)

'''
======================================================================================================================================
                                    FOR CBM | ON DEMAND SEND PRODUCTION ORDER DETAILS FOR CHARTS                    -VATSAL
======================================================================================================================================
'''
@app.route('/PO_FOR_CHART', methods=['GET', 'POST'])
def post_production_orders_for_chart():
    if request.method == 'POST':
        print("CBM P_O FOR CHARTS REQUESTED!")
        data = request.get_json(force=True)
        print(data)
        from_time =  data["starttime"]
        to_time = data["stoptime"]
        machine = data["asset"]
        response = []
        response = new_database_operations.get_active_production_orders_from_to(from_time,to_time,machine)
        print(response)
        for record in response:

            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass

        print("PART DETAILS SENT TO CBM-CHARTS")

    return jsonify(response)


@app.route('/GET_SHIFT_DETAILS_FOR_PQCR', methods=['GET', 'POST'])
def post_production_orders_for_pqcr():
    if request.method == 'POST':
        print("CBM - PRODUCTION ORDER DETAILS FOR PQCR REQUESTED!")
        data = request.get_json(force=True)
        print(data)
        from_time =  data["starttime"]
        to_time = data["stoptime"]
        machine = data["asset"]
        response = []
        response = new_database_operations.get_active_production_orders_from_to_for_pqcr(from_time,to_time,machine)
        print(response)
        for record in response:

            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass

        print("PART DETAILS SENT TO CBM-PQCR")

    return jsonify(response)


@app.route('/live_done_orders', methods=['GET', 'POST'])
def send_orders_to_trace():
    if request.method == 'POST':
        print("TRACE REQUESTED ORDERS FOR HANDHELD")
        data = request.get_json(force=True)
        response = new_database_operations.get_orders_for_trace()

    return response





# ======================================================================================================================================

@socketio.on("save_part_weight_and_cavities")
def save_part_weight_and_cavities(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data)
        new_database_operations.update_specific_production_order(data)
        print("Updated Data !")

@socketio.on("save_energy_details")
def add_energy_detail(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data)
        new_database_operations.add_energy_details(data)
        print("Updated Data !")




@app.route('/production_plan')
def index():
    if 'user' in session and 'login' in session:
        if session['login']:

            return render_template("production_plan.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename="",
                                   phone = session["user"]
                                   )


    else:
        return redirect(url_for('login'))


@app.route('/done_production_orders')
def done_production_orders():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("done_production_plan.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],

                                   )
    else:
        return redirect(url_for('login'))


@app.route('/overview_plan')
def overview_plan():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("overview_plan.html",
                                   active_page='shift_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename="",
                                   phone=session["user"]
                                   )

@app.route('/history')
def history_details():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("history1.html",
                                   active_page='shift_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename="",
                                   phone=session["user"]
                                   )

@app.route('/assembly')
def assembly_details():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("assembly.html",
                                   active_page='shift_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename="",
                                   phone=session["user"]
                                   )

@app.route('/vehicle')
def vehicle_details():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("vehicle.html",
                                   active_page='shift_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename="",
                                   phone=session["user"]
                                   )

@app.route('/work')
def work_details():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("Work.html",
                                   active_page='shift_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename="",
                                   phone=session["user"]
                                   )


@socketio.on('savedetails')
def save_details(data):
    print("data!!!!!",data)
    new_database_operations.add_data(data)

@socketio.on('task_data')
def data_details():
    task = new_database_operations.display_task_table()
    socketio.emit("task_table",task)









@app.route('/upload_production_plan')
def upload_production_plan():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("upload_production_plan.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/upload_production_plan_2')
def upload_production_plan_2():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("upload_production_plan_2.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/upload_production_plan_3')
def upload_production_plan_3():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("upload_production_plan_3.html",
                                   active_page='production_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename=""
                                   )
    else:
        return redirect(url_for('login'))


# ===============================================================
@app.route('/shift_plan')
def shift_plan():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("shift_plan.html",
                                   active_page='shift_plan',
                                   role=session['role'],
                                   name=session['name'],
                                   file_status="Please Select a File to continue.",
                                   filename="",
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_shift_records")
def get_shift_records(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")

        shift_records = new_database_operations.get_shift_records_without_kpi(data)

        # Seriallize Planned Start Time and End Time

        for record in shift_records:
            try:
                current_time = datetime.datetime.now()
                if current_time < record["SHIFT START TIME"]:
                    record["STATUS"] = "Planned"
                if record["SHIFT START TIME"] <= current_time <= record["SHIFT STOP TIME"]:
                    record["STATUS"] = "Live"
                if current_time > record["SHIFT STOP TIME"]:
                    record["STATUS"] = "Done"
                record["SHIFT START TIME"] = record["SHIFT START TIME"].strftime("%Y-%m-%d %H:%M:%S")
                record["SHIFT STOP TIME"] = record["SHIFT STOP TIME"].strftime("%Y-%m-%d %H:%M:%S")
            except:
                pass
        print(shift_records)
        socketio.emit("sent_shift_records", shift_records, room=room)
        print("SHIFT RECORDS SENT ")


@socketio.on("get_shift_plan_dates")
def get_shift_plan_dates():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        temp = datetime.datetime.now() + datetime.timedelta(minutes=constants.timezone_offset)
        to_send = {}

        to_send["todays_date"] = temp.strftime("%Y-%m-%d")
        to_send["tomorrows_date"] = (temp + datetime.timedelta(hours=24)).strftime("%Y-%m-%d")

        print(to_send)
        socketio.emit("send_shift_plan_dates", to_send, room=room)
        print("SHIFT DATES SENT ")


@socketio.on("save_shift_data")
def save_shift_data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data)
        new_database_operations.update_shift_records(data)
        # print(to_send)

        print("SHIFT DATA UPDATED")


@socketio.on("get_specific_shift_record")
def get_specific_shift_record(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        print(data)
        record = new_database_operations.get_specific_shift_record(data)
        print(record)
        try:
            current_time = datetime.datetime.now()
            if current_time < record["SHIFT START TIME"]:
                record["STATUS"] = "Planned"
            if record["SHIFT START TIME"] <= current_time <= record["SHIFT STOP TIME"]:
                record["STATUS"] = "Live"
            if current_time > record["SHIFT STOP TIME"]:
                record["STATUS"] = "Done"
            record["SHIFT START TIME"] = record["SHIFT START TIME"].strftime("%Y-%m-%d %H:%M:%S")
            record["SHIFT STOP TIME"] = record["SHIFT STOP TIME"].strftime("%Y-%m-%d %H:%M:%S")
        except:
            pass

        socketio.emit("send_specific_shift_record", record, room=room)
        print("SPECIFIC SHIFT DATA SENT ")


'''
=================================================================================================================
DOWNTIME
=================================================================================================================
'''


@app.route('/downtime_post', methods=['POST', 'GET'])
def downtime_post():
    if request.method == 'POST':
        data = request.json
        print(data)
        # check if production order is on that machine
        production_order = new_database_operations.get_production_order_from_machine(data)
        if production_order is not None:
            if len(data["data"]) > 0:
                for downtime_record in data["data"]:
                    downtime_record["ORDER NUMBER (LEFT)"] = production_order["ORDER NUMBER (LEFT)"]
                    downtime_record["ORDER NUMBER (RIGHT)"] = production_order["ORDER NUMBER (RIGHT)"]
                    to_send_back = new_database_operations.update_downtime(downtime_record)
                    print("SOFTWARE DOWNTIME RECORDED !")

        print("Data Recevied")
        return "Data Recieved"
    else:
        return "wrong_method"


@socketio.on("get_current_time_start")
def get_current_time_start(json):

    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        temp = datetime.datetime.now() + datetime.timedelta(minutes=constants.timezone_offset)
        data = json["data"]
        downtime_status = json["downtime_status"]

        # Maintain current Downtime Status
        downtime_status["downtime_start"] = temp.strftime(constants.time_format)
        downtime_status["downtime_status"] = "true"
        downtime_status["downtime_stop"] = ""
        downtime_status["downtime_type"] = downtime_status["downtime_type"]

        print(data)
        print(downtime_status)

        new_database_operations.start_current_downtime_status(data,downtime_status)
        socketio.emit("send_current_downtime_status", downtime_status , room=room)
        print("CURRENT TIME SENT 1!")


@socketio.on("get_current_time_stop")
def get_current_time_stop(json):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        temp = datetime.datetime.now() + datetime.timedelta(minutes=constants.timezone_offset)
        data = json["data"]
        downtime_status = json["downtime_status"]

        # Maintain current Downtime Status
        downtime_status["downtime_stop"] = temp.strftime(constants.time_format)
        downtime_status["downtime_status"] = "true"
        downtime_status["downtime_type"] = downtime_status["downtime_type"]

        print(data)
        print(downtime_status)

        new_database_operations.stop_current_downtime_status(data, downtime_status)

        #Maintain current Downtime Status
        current_downtime_status = new_database_operations.get_current_downtime_status(data)
        socketio.emit("send_current_downtime_status", current_downtime_status, room=room)
        print("CURRENT TIME SENT 5!")

@socketio.on("get_current_downtime_status")
def get_current_downtime_status(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("------ GET CURRENT DOWNTIME STATUS--------")

        print(data)
        print("-------------------------------------------")

        # Maintain current Downtime Status
        current_downtime_status = new_database_operations.get_current_downtime_status(data)
        print(current_downtime_status)
        socketio.emit("send_current_downtime_status", current_downtime_status, room=room)
        print("CURRENT TIME SENT 4!")

@socketio.on("get_downtime_record")
def get_downtime_record(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("console_2", "Recording Downtime !", room=room)
        print("-------UPDATING DOWNTIME---------")
        print(data)
        print("---------------------------------")
        record,state = new_database_operations.update_downtime(data)

        if state :
            # send complete production order back


            record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass

            socketio.emit("send_production_order", record, room=room)
            time.sleep(1)
            socketio.emit("console_2", "Downtime Successfully Recorded !", room=room)

            # Update Downtime Chart
            to_send_back = new_database_operations.update_downtime_charts(record)
            socketio.emit("downtime_chart_update", to_send_back, room=room)

        # clear the current downtime status from database
        downtime_status = {
            "downtime_start": "",
            "downtime_stop": "",
            "downtime_status": "false",
            "downtime_type": "",
        }
        new_database_operations.reset_downtime_status(data, downtime_status)
        # Maintain current Downtime Status
        current_downtime_status = new_database_operations.get_current_downtime_status(data)
        socketio.emit("send_current_downtime_status", current_downtime_status, room=room)
        print("DOWNTIME RECORDED 3!")

@socketio.on("get_current_time_clear")
def get_current_time_clear(json):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("============================")
        data = json["data"]

        # Maintain current Downtime Status
        downtime_status = {
            "downtime_start": "",
            "downtime_stop": "",
            "downtime_status": "false",
            "downtime_type": "",
        }
        new_database_operations.stop_current_downtime_status(data, downtime_status)

        # Maintain current Downtime Status
        current_downtime_status = new_database_operations.get_current_downtime_status(data)
        socketio.emit("send_current_downtime_status", current_downtime_status, room=room)
        print("CURRENT TIME SENT 2!")

        socketio.emit("console_2", "Downtime cleared !", room=room)


@socketio.on("update_downtime_charts")
def update_downtime_charts(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        to_send_back = new_database_operations.update_downtime_charts(data)
        socketio.emit("downtime_chart_update", to_send_back, room=room)
        time.sleep(2)
        #socketio.emit("console_2", "Downtime Successfully Recorded !", room=room)
        print("DOWNTIME CHART UPDATED!")


@socketio.on("change_downtime_status")
def change_downtime_status(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        new_database_operations.update_downtime_status(data)
        print("Done Updating Downtime !")


@socketio.on("change_downtime_type")
def change_downtime_type(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        new_database_operations.update_downtime_type(data)
        print("Done Updating Downtime !")


@socketio.on("get_downtime_sum")
def get_downtime_sum(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]

        downtime_sum = new_database_operations.get_downtime_chart_sum(req_date,shift_id)
        socketio.emit("sent_downtime_sum",downtime_sum)
        print("Downtime Sum sent!")


@socketio.on("get_downtime_by_machine")
def get_downtime_by_machine(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]
        machine = data["machine"]

        downtime_by_machine = new_database_operations.get_downtime_chart_by_machine(req_date,shift_id,machine)
        socketio.emit("sent_downtime_by_machine",downtime_by_machine)
        print("Downtime Sum sent!")

@socketio.on("get_timeline_by_machine")
def get_timeline_by_machine(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]
        machine = data["machine"]

        timeline = new_database_operations.get_shift_timeline(req_date,shift_id,machine)
        socketio.emit("sent_timeline_by_machine",timeline)
        print("TIMELINE DATA SENT")


@socketio.on("get_all_timelines")
def get_all_timelines(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]


        timeline = new_database_operations.get_timelines_for_all_machines(req_date,shift_id)
        socketio.emit("sent_all_timelines",timeline)
        print("TIMELINE DATA SENT")


@app.route('/downtime', methods=['GET', 'POST'])
def downtime():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("downtime_analysis.html",
                                   active_page='downtime',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


'''
=================================================================================================================
REJECTION
=================================================================================================================
'''
@socketio.on("add_new_rejection_record")
def add_new_rejection_record(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        state,record = new_database_operations.add_new_rejection_record(data)

        if state:
            record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass
            socketio.emit("send_production_order",record,room=room)
        print("NEW REJECTION RECORD ADDED")


@socketio.on("add_compound_detail")
def add_compound_detail(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        state,record = new_database_operations.add_compound_detail(data)

        if state:
            record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass
            socketio.emit("send_production_order",record,room=room)
        else:
            print("Adding compound details failed!")
        print("COMPOUND RECORD ADDED")

@socketio.on("rejection_details_records")
def rejection_details_records(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("rejection_console_update", "", room=room)
        to_send_back = new_database_operations.update_rejection(data)
        socketio.emit("rejection_chart_update", to_send_back, room=room)
        time.sleep(2)
        socketio.emit("rejection_console_update", "Rejection Successfully Recorded !", room=room)
        print("REJECTION CHART UPDATED!")


@socketio.on("startup_rejection_details_records")
def startup_rejection_details_records(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("startup_rejection_details_console", "", room=room)
        to_send_back = new_database_operations.update_startup_rejection(data)
        socketio.emit("startup_rejection_values_update", to_send_back, room=room)
        time.sleep(2)
        socketio.emit("startup_rejection_details_console", "Rejection Successfully Recorded !", room=room)
        print("START UP REJECTION UPDATED!")


@socketio.on("record_process_scrap")
def record_process_scrap(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        socketio.emit("record_process_scrap_console", "", room=room)
        new_database_operations.update_process_scrap(data)
        time.sleep(1)
        socketio.emit("record_process_scrap_console", "Process Scrap Updated Successfully !", room=room)

        print("PROCESS SCRAP UPDATED!")


@socketio.on("update_rejection_chart")
def update_rejection_chart(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        to_send_back = new_database_operations.update_rejection_chart(data)
        time.sleep(1)
        socketio.emit("rejection_chart_update", to_send_back, room=room)
        print("REJECTION CHART UPDATED!")


@socketio.on("update_pie_chart")
def update_pie_chart(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        to_send_back = new_database_operations.update_rejection_piechart(data)
        time.sleep(1)
        socketio.emit("rejection_piechart_update", to_send_back, room=room)
        print("REJECTION PIE CHART UPDATED!")


@app.route('/rejection', methods=['GET', 'POST'])
def rejection():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("rejection_analysis.html",
                                   active_page='rejection',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("change_rejection_status")
def change_rejection_status(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        new_database_operations.update_rejection_status(data)
        print("Done Updating Rejection !")
#------------------------------------FOR PLANT ANALYSIS PAGE-----------------------------------------------
@socketio.on("get_production_plan_analysis_for_shift")
def get_production_plan_analysis_for_shift(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]

        chart_data = new_database_operations.production_plan_analysis_for_shift(req_date,shift_id)
        socketio.emit("sent_production_plan_analysis_for_shift",chart_data)
        print("production plan analysis for shift sent")

@socketio.on("get_energy_chart")
def get_energy_chart(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]

        chart_data = new_database_operations.get_energy_consumption_for_shift(req_date,shift_id)
        socketio.emit("sent_energy_chart",chart_data)
        print("get_energy_chart")

@socketio.on("get_kpi_chart")
def get_kpi_chart(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]

        chart_data = new_database_operations.get_kpi_chart(req_date,shift_id)
        socketio.emit("sent_kpi_chart",chart_data)
        print("kpi chart sent")



@socketio.on("get_line_rejection_sum")
def get_line_rejection_sum(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]


        line_rejection_sum = new_database_operations.get_line_rejection_sum(req_date,shift_id)
        socketio.emit("sent_line_rejection_sum",line_rejection_sum)
        print("line_rejection Sum sent!")

@socketio.on("get_line_rejection_by_machine")
def get_line_rejection_by_machine(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]
        machine = data["machine"]

        rej_by_machine = new_database_operations.get_line_rejection_by_machine(req_date,shift_id,machine)
        socketio.emit("sent_line_rejection_by_machine",rej_by_machine)
        print("rejection_by_machine sent!")


@socketio.on("get_rejection_sum")
def get_rejection_sum(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]


        line_rejection_sum = new_database_operations.get_rejection_sum(req_date,shift_id)
        socketio.emit("sent_rejection_sum",line_rejection_sum)
        print("rejection Sum sent!")


@socketio.on("get_rejection_by_machine")
def get_rejection_by_machine(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        req_date = data["date"]
        shift_id = data["shift"]
        machine = data["machine"]

        rej_by_machine = new_database_operations.get_rejection_by_machine(req_date,shift_id,machine)
        socketio.emit("sent_rejection_by_machine",rej_by_machine)
        print("rejection_by_machine sent!")

@socketio.on("active_shift_id")
def active_shift_id():
    if 'user' in session:
        room = request.sid
        join_room(room)
        current_time = datetime.datetime.now()
        ct = datetime.datetime.strftime(current_time, "%d/%m/%Y, ")
        ct2 = datetime.datetime.strptime(ct + "6:00:00", constants.time_format)
        shiftA_start = ct2
        shiftA_stop = shiftA_start + datetime.timedelta(minutes=480)
        shiftB_start = shiftA_stop
        shiftB_stop = shiftB_start + datetime.timedelta(minutes=480)
        shiftC_start = shiftB_stop
        shiftC_stop = shiftC_start + datetime.timedelta(minutes=480)
        shift_id = ""
        if shiftA_start <= current_time < shiftA_stop:
            shift_id = "A"

        elif shiftB_start <= current_time < shiftB_stop:
            shift_id = "B"

        elif shiftC_start <= current_time < shiftC_stop:
            shift_id = "C"


        socketio.emit("sent_active_shift_id",shift_id)
        print(shift_id)


'''
=================================================================================================================
BASIC DETAILS
=================================================================================================================
'''


@app.route('/shot_data_post', methods=['POST', 'GET'])
def shot_post():
    if request.method == 'POST':
        data = request.json
        print("---------- SHOTS RECEIVED ------")


        data = new_database_operations.filter_duplicate_shots(data)

        # UPDATE COUNTER
        new_data = {}
        new_data['machine'] = data['machine']
        new_data['new_shots'] = data['total_shots']
        new_data['timestamp'] = data['timestamp']

        # Check if there is a production order on that machine
        new_database_operations.update_shot_data(new_data)
        production_order = new_database_operations.get_production_order_from_machine(data)
        if production_order is not None:
            if len(data["shot_details"]) > 0:
                for shot_record in data["shot_details"]:
                    shot_record["ORDER NUMBER (LEFT)"] = production_order["ORDER NUMBER (LEFT)"]
                    shot_record["ORDER NUMBER (RIGHT)"] = production_order["ORDER NUMBER (RIGHT)"]
                    to_send_back = new_database_operations.update_shot_details(shot_record)



                    # Add time difference as downtime if the actual cycle time exceeds ideal cycle time.
                    new_database_operations.update_minor_stops(shot_record)


        #print("Data Recevied")
        return "Data received"


    else:
        return "wrong_method"


@app.route('/shot_data', methods=['POST', 'GET'])
def add_shot_data():
    if request.method == 'POST':
        return "wrong_method"
    else:
        data = {}
        data['machine'] = request.args.get('machine')
        data['new_shots'] = request.args.get('new_shots')
        data['timestamp'] = request.args.get('timestamp')

        # Check if there is a production order on that machine
        new_database_operations.update_shot_data(data)

        print(" ---------- SHOT DATA RECEIVED -------------------- ")
        print(data)
        print(" -------------------------------------------------- ")

        return "data recieved"


@app.route('/basic', methods=['GET', 'POST'])
def basic():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("basic_details_analysis.html",
                                   active_page='basic',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_shot_details_by_machine")
def get_shot_by_machine(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        #print("============================")
        # QUERY FORMAT
        record, found = new_database_operations.get_shot_details_by_machine(data)
        if found:
            #print("FOUND IN_PROCESS PRODUCTION ORDER")
            socketio.emit("console", "Production Order In Process", room=room)
            time.sleep(0.5)
            record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass
            #print("----------------")
            #print(record)
            #print("-----------------")
            socketio.emit("send_production_order", record, room=room)
        else:
            print("PRODUCTION ORDER NOT FOUND !")
            socketio.emit("send_production_order", {}, room=room)
            socketio.emit("console", "No Production Order", room=room)
            time.sleep(0.5)


'''
=================================================================================================================
GENERIC DETAILS
=================================================================================================================
'''


@app.route('/plant_details')
def plant_details():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("ppap_dash.html",
                                   active_page='plant_details',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


'''
=================================================================================================================
SHIFT PRODUCTION REPORT
=================================================================================================================
'''


@app.route('/spr')
def spr():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("SPR1.html",
                                   active_page='spr',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/SPR_template/<shift_date>/<shift_id>/<machine>')
def SPR_template(shift_date, shift_id, machine):
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("SPR_template.html",
                                   active_page='spr',
                                   role=session['role'],
                                   name=session['name'],
                                   shift_date=shift_date,
                                   shift_id=shift_id,
                                   machine=machine
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_SPR_data")
def get_SPR_data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        if "SHIFT DATE" in data:
            data["SHIFT DATE"] = data["SHIFT DATE"].replace("-", "/")
            print(data)
            # new_database_operations.update_rejection_status(data)
            SPR_DATA = new_database_operations.get_shift_report(data)
            socketio.emit("send_SPR_data", SPR_DATA, room=room)
        print("Done Sending Shift Details !")

@app.before_first_request
def activate_job():
    def run_job():
        while True:
            print("RUNNING CRON JOB")
            current_time = datetime.datetime.now()
            new_database_operations.cron_job(current_time)
            time.sleep(constants.cron_job_time)

    def run_kpi_cronjob():
        while True:
            print("KPI RUNNING CRON JOB")
            current_time = datetime.datetime.now()
            try:
                new_database_operations.kpi_cron_job(current_time)
            except Exception as Argument:
                f = open("cron_job_error.txt", "a")
                f.write(str(Argument) + current_time.strftime(constants.time_format))
                f.close()
            time.sleep(constants.cron_job_time)

    thread_kpi = threading.Thread(target=run_kpi_cronjob)
    thread = threading.Thread(target=run_job)
    thread.start()
    thread_kpi.start()


@socketio.on("get_current_shift_SPR_page")
def get_current_shift_SPR_page():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("------------------------------------------------------GET CURRENT SHIFT SPR "
              "PAGE-------------------------------------------")
        shift = new_database_operations.get_current_shift_details()
        to_send = {
            "SHIFT DATE": shift["SHIFT DATE"],
            "SHIFT ID": shift["SHIFT ID"]
        }
        socketio.emit("shift_details_SPR_page", to_send, room=room)


'''
=================================================================================================================
PQCR
=================================================================================================================
'''


@app.route('/pqcr')
def pqcr():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("PQCR1.html",
                                   active_page='pqcr',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/pqcr_template/<shift_date>/<machine>')
def pqcr_template(shift_date, machine):
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("pqcr_template.html",
                                   active_page='spr',
                                   role=session['role'],
                                   name=session['name'],
                                   shift_date=shift_date,
                                   # shift_id = shift_id,
                                   machine=machine
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("flaash")
def flaash(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)

        fname = os.path.join(app.static_folder, 'data.json')
        with open(fname) as json_file:
            data1 = json.load(json_file)
        socketio.emit("show_json", data1)


@app.route('/pqcr_data_post', methods=['POST', 'GET'])
def pqcr_post():
    if request.method == 'POST':
        data = request.json
        print(data)
        try:
            data["timestamp"] = datetime.datetime.strptime(data["timestamp"], constants.time_format)
            data["PQCR DATA"]["timestamp"] = datetime.datetime.strptime(data["PQCR DATA"]["timestamp"],
                                                                        constants.time_format)
            new_database_operations.insert_PQCR_data(data)
            print("Data Recevied")
        except:
            pass
        return "Data Recieved"
    else:
        return "wrong_method"


@socketio.on("generate_PQCR_data")
def generate_PQCR_Data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print(data)
        data["SHIFT DATE"] = data["SHIFT DATE"].replace("-", '/')
        print(data["SHIFT DATE"])
        PQCR_data = new_database_operations.generate_PQCR_report(data["SHIFT DATE"], data["MACHINE"])
        socketio.emit("send_PQCR_data", PQCR_data)


@socketio.on("get_PQCR_chart_data")
def get_PQCR_chart_data(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("PQCR DATA REQUESTED .")
        print(data)
        PQCR_data = new_database_operations.PQCR_chart_data(data)
        socketio.emit("send_PQCR_chart_data", PQCR_data)


@app.route('/charts')
def charts():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("live.html",
                                   active_page='charts',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("get_chart_refresh_rate")
def get_chart_refresh_rate():
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("PQCR REFRESH RATE")
        PQCR_data = {"refresh_rate": constants.PQCR_chart_refresh_time}
        socketio.emit("send_chart_refresh_rate", PQCR_data)


'''
=================================================================================================================
HEIRARCHY PAGE 
=================================================================================================================
'''


@app.route('/hierarchy')
def hierarchy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("hirarchy.html",
                                   active_page='hierarchy',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))



@socketio.on("get_heirarchy_data")
def get_heirarchy_data():
    if 'user' in session:
        room = request.sid
        join_room(room)
        data_json = new_database_operations.get_json_for_heirarchy_chart()
        to_send = {
            "update_time" : datetime.datetime.now().strftime(constants.time_format),
            "data_json" : data_json
        }
        socketio.emit("send_heirarchy_data", to_send , room=room)
        print("SENT HEIRARCHY DATA !")


'''
=================================================================================================================
TO DO 
=================================================================================================================
'''


@app.route('/quality')
def quality():
    return redirect(url_for('plant_details'))


@app.route('/all_apps')
def all_apps():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("all_apps.html",
                                   active_page='all_apps',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session['user']
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/terms_and_conditions')
def terms_and_conditions():
    return render_template("terms_and_conditions.html")


@app.route('/supportandfaq')
def supportandfaq():
    return render_template("support_and_FAQ.html")



@socketio.on("FAQ_data")
def FAQ_data(data):
        print(data)
        Val = database_operations.create_FAQ(data)
        #print(Val)
        if Val==1:
            print("done")
        socketio.emit("support_FAQ_data",Val)


# ===================================================================

@app.route('/RFID_DATA', methods=['GET', 'POST'])
def rfid_test():
    if request.method == 'POST':
        sessions_data = request.get_json(force=True)
        print("======================Received Data ============", sessions_data)

        current_time = datetime.datetime.now().strftime(constants.spr_time_format)
        tags_save_path = "../RFID SCANS/"
        filename = tags_save_path + current_time + ".json"
        with open(filename, 'w') as fp:
            json.dump(sessions_data, fp)
        # db_operation.bin_updator(sessions_data)
        # socketio.emit("gate_blinker", sessions_data["bins"]["movement"]["gate"])
    return "RFID TAGS DATA RECEIVED"


'''@app.route('/REPORT/<jobnumber>')
def report(jobnumber):
    print("Report Requested")
    print(jobnumber)
    print("----------------")

    job_details = database.query_jobdetails({"jobnumber": str(jobnumber)})
    job_details = job_details[0]
    print(job_details)
    print(job_details["jobtype"])

    # job_data = database.get_job_data(jobnumber=str(jobnumber))
    # print(job_data)

    return render_template('REPORTS/iframe_trial.html',
                           src="/Report/" + job_details["jobtype"] + "/" + str(jobnumber))'''

#=======================================sarika changes===================================================

@socketio.on("process_scrap")
def process_scrap(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("================",data)
        new_data= new_database_operations.insert_process_scrap(data)
        find_data=new_database_operations.find_process_scrap(data)
        socketio.emit("process_scrap_records",find_data)
        

@app.route('/quality_inspection')
def quality_inspection():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("quality_inspection.html",
                                   active_page='charts',
                                   role=session['role'],
                                   name=session['name'],
                                   phone = session["user"]
                                   )
    else:
        return redirect(url_for('login'))

@app.route('/partnumber_details')
def partnumber_details():
    if 'user' in session and 'login' in session:
        if session['login']:

            return render_template("partnumber_details.html",
                                   active_page='partno_details',
                                   role=session['role'],
                                   name=session['name'],
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("part_number_details_page")
def part_number_details_page(data):
    if 'user' in session:
        room = request.sid
        join_room(room)

        partnumberdetails = part_number_details.query_partnumberdetails()

        socketio.emit("sent_part_number_details", partnumberdetails)

@app.route('/binnumber_details/<partnumber>')
def binnumber_details(partnumber):
    if 'user' in session and 'login' in session:
        if session['login']:

            result = partnumber

            return render_template("binnumber_details.html",
                                   active_page='partno_details',
                                   role=session['role'],
                                   name=session['name'],
                                   part_number = result
                                   )
    else:
        return redirect(url_for('login'))

@socketio.on("bin_number_details_page")
def bin_number_details_page(data):

    if 'user' in session:
        room = request.sid
        join_room(room)

        binnumberdetails = part_number_details.query_binnumberdetails(data)

        socketio.emit("sent_bin_number_details", binnumberdetails)



@socketio.on("hierarchy_json_read")
def hierarchy_json_read():
        if 'user' in session:
            room = request.sid
            join_room(room)
            print("json read")
            fname = os.path.join(app.static_folder, 'hierarchy_data.json')
            with open(fname) as json_file:
                data1 = json.load(json_file)
                print(data1)
            socketio.emit("hierarchy_show_json", data1)


#===================================plant analysis=======================================
'''@socketio.on("plant_analysis")
def plant_analysis():
        if 'user' in session:
            room = request.sid
            join_room(room)
            print("json read")
            fname = os.path.join(app.static_folder, 'export.json')
            with open(fname) as json_file:
                data1 = json.load(json_file)
                print("json data",data1)
                kpi_data= data1["kpi"]
                print("kpi data",kpi_data)
                #list(dict)[-1]
                val=len(kpi_data)
                print(val)
                json_data=[]
                val1 = val - 10
                print(val)

                for i in range (val1,len (kpi_data)):
                    json_data.append(kpi_data[i])

                print(json_data)
            socketio.emit("export_json", json_data) '''


#============================================dhanashri's contribution  plant analysis================================

@socketio.on("plant_analysis")
def plant_analysis():
        if 'user' in session:
            room = request.sid
            join_room(room)
            print("json read")
            fname = os.path.join(app.static_folder, 'export.json')
            with open(fname) as json_file:
                data1 = json.load(json_file)
                #print("json data",data1)
                kpi_data= data1["kpi"]
                #print("kpi data",kpi_data)
                #list(dict)[-1]
                val=len(kpi_data)
                print(val)
                json_data=[]
                val1 = val - 10
                print(val)

                for i in range (val1,len (kpi_data)):
                    json_data.append(kpi_data[i])

                #print(json_data)
            socketio.emit("export_json", json_data)


@socketio.on("plant_analysis_by_date")
def plant_analysis_by_date(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("==============active prod orders  =============")
        production_orders = new_database_operations.get_active_production_orders()
        #print(data)
        #data["from_date"]= data["from_date"].replace('T','')
        #data["to_date"] = data["to_date"].replace('T','')
        from_date = datetime.datetime.strptime(data["from_date"].strip(' \t\r\n'), "%Y-%m-%dT%H:%M:%S")
        to_date = datetime.datetime.strptime(data["to_date"].strip(' \t\r\n'), "%Y-%m-%dT%H:%M:%S")

        # print(type(from_date))
        # print(type(to_date))
        #print("prod orders", production_orders)
        #if from_date.time() < to_date.time():
            #print("from date is greater")
        print("before loop")
        #print(production_orders)
        print(len(production_orders))
        No_of_machines =len(production_orders)
        length_kpi=[]
        temp= []
        for i in range(0,len(production_orders)):
            length=len(production_orders[i]["kpi"])
            print(length)
            count=0
            temp_list= [
                {
                  'ORDER NUMBER (LEFT)':production_orders[i]["ORDER NUMBER (LEFT)"],
                  'MACHINE' : production_orders[i]["MACHINE"],
                }
            ]
            for j in range(0,length):
                #print("type of kpi time",i,j,production_orders[i]["kpi"][j]["Time"])
                if type(production_orders[i]["kpi"][j]["Time"]) == str:
                    production_orders[i]["kpi"][j]["Time"] = datetime.datetime.strptime(production_orders[i]["kpi"][j]["Time"], "%d/%m/%Y, %H:%M:%S")
                    if(from_date <= production_orders[i]["kpi"][j]["Time"]  <= to_date):
                            print("date is in between ")
                            temp[i].append([production_orders[i]["kpi"][j]])
                    else:
                            print("No kpi available")

                else:
                        if from_date <= production_orders[i]["kpi"][j]["Time"]  <= to_date:
                                #print("date is in between ")
                                temp_list.append(production_orders[i]["kpi"][j])
                                count= count + 1
                                length_kpi.append(count)



            temp.append(temp_list)



        print("temp ",temp)
        max_kpi=max(length_kpi)
        #print("length of temp ", len(temp))
        print("printing temp element",temp[0][1]["Oee"])
        print("------------------------------------")
        print("printing temp element",temp[1][1])



        get_oee = []
        oee_add = 0
        #cnt= 1
        for k in range(1,max_kpi):

            for x in range(0,len(temp)):
                if temp[x][k]["Oee"] == 'NA':
                    temp[x][k]["Oee"] = 0
                    oee_add = oee_add + temp[x][k]["Oee"]

                else:
                    oee_add = oee_add + temp[x][k]["Oee"]

                oee_avg= oee_add /No_of_machines
                #print("oee_avg",oee_avg)
                get_oee.append(oee_avg)


        # print(get_oee)


        get_productivity = []
        productivity_add = 0
        #cnt= 1
        for k in range(1,max_kpi):

            for x in range(0,len(temp)):
                if temp[x][k]["Productivity"] == 'NA':
                    temp[x][k]["Productivity"] = 0
                    productivity_add = productivity_add + temp[x][k]["Productivity"]

                else:
                    productivity_add = productivity_add + temp[x][k]["Productivity"]

                Production_avg= productivity_add /No_of_machines
                #print("productivity_avg",Production_avg)
                get_productivity.append(Production_avg)


        # print(get_productivity)


        get_Quality = []
        Quality_add = 0
        #cnt= 1
        for k in range(1,max_kpi):

            for x in range(0,len(temp)):
                if temp[x][k]["Quality"] == 'NA':
                    temp[x][k]["Quality"] = 0
                    Quality_add = Quality_add + temp[x][k]["Quality"]

                else:
                    Quality_add = Quality_add + temp[x][k]["Quality"]

                Quality_avg= Quality_add /No_of_machines
                #print("Quality_avg",Quality_avg)
                get_Quality.append(Quality_avg)


        # print(get_Quality)


        get_Availability = []
        Availability_add = 0
        #cnt= 1
        for k in range(1,max_kpi):

            for x in range(0,len(temp)):
                if temp[x][k]["Availability"] == 'NA':
                    temp[x][k]["Availability"] = 0
                    Availability_add = Availability_add + temp[x][k]["Availability"]

                else:
                    Availability_add = Availability_add + temp[x][k]["Availability"]

                Availability_avg= Availability_add /No_of_machines
                #print("Availability_avg",Availability_avg)
                get_Availability.append(Availability_avg)


        # print(get_Availability)


        all_data_list =[]
        for p in range(0,max_kpi):
            all_data_dict= {
                  'Oee':get_oee[p],
                  'Productivity' : get_productivity[p],
                  'Quality' : get_Quality[p],
                  'Availability' : get_Availability[p]

                    }

            all_data_list.append(all_data_dict)

        # print("=======================")
        #print("all data",all_data_list)
        socketio.emit("export_json", all_data_list)


@app.route("/verify_login" , methods=['GET','POST'])
def verify_login():
    if request.method == 'POST':
        data= request.get_json(force=True)
        print(data)

        Val = database_operations.verify_credentials(data)
        if Val == 2:
            session["user"] = data["phone"]

            data1 = database_operations.return_credentials(data['phone'])
            print(type(data1))
            session['role'] = data1['role']
            session['name'] = data1['name']
            session['o_id'] = data1['o_id']
            session["login"] = True
            d1= {
                'role' : session['role'],
                'name':session['name'],
                'response' : 2
            }
            return d1
        if Val == 1:
            session["user"] = data["phone"]
            # session['role'] = data["role"]
            data1 = database_operations.return_credentials(data['phone'])
            session['role'] = data1['role']
            session['name'] = data1['name']
            session['o_id'] = data1['o_id']
            session["login"] = False
            return "1"
        if Val == 0:
            return "0"
            
# ROUTES FOR HISTORY PAGES || DONE PRODUCTION ORDERS >> DETAILS
@app.route('/basic_details/<orders>')
def basic_details_history(orders):
    if 'user' in session and 'login' in session:
        if session['login']:

            res = orders.split(",")
            order_left = res[0]
            order_right = res[1]


            return render_template("basic_details_history.html",
                                   active_page='plant_details',
                                   role=session['role'],
                                   name=session['name'],
                                   order_number_left = order_left,
                                   order_number_right = order_right,
                                   phone=session["user"]
                                   )
    else:
        return redirect(url_for('login'))

@socketio.on("get_shot_details_by_machine_history")
def get_shot_by_machine_history(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        print("############################################################")
        order_left = data["order_number_left"]
        order_right = data["order_number_right"]
        #print("============================")
        # QUERY FORMAT
        record, found = new_database_operations.get_shot_details_by_machine_history(order_left,order_right)
        if found:
            #print("FOUND IN_PROCESS PRODUCTION ORDER")
            socketio.emit("console", "Done", room=room)
            time.sleep(0.5)
            record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass
            #print("----------------")
            #print(record)
            #print("-----------------")
            socketio.emit("send_production_order", record, room=room)
        else:
            print("PRODUCTION ORDER NOT FOUND------- !")
            socketio.emit("send_production_order", {}, room=room)
            socketio.emit("console", "No Production Order", room=room)
            time.sleep(0.5)


@socketio.on("get_production_order_details_by_order_number")
def get_production_order_details_by_order_number(data):
    if 'user' in session:
        room = request.sid
        join_room(room)
        order_left = data["order_number_left"]
        order_right = data["order_number_right"]
        #print("============================")
        # QUERY FORMAT
        record, found = new_database_operations.get_production_order_details_by_order_number(order_left,order_right)
        if found:
            #print("FOUND IN_PROCESS PRODUCTION ORDER")
            socketio.emit("console", "Done", room=room)
            time.sleep(0.5)
            record["PLANNED_START"] = record["PLANNED_START"].strftime(constants.time_format)
            record["PLANNED_STOP"] = record["PLANNED_STOP"].strftime(constants.time_format)
            try:
                record["ACTUAL_START"] = record["ACTUAL_START"].strftime(constants.time_format)
                record["ACTUAL_STOP"] = record["ACTUAL_STOP"].strftime(constants.time_format)
            except:
                pass
            #print("----------------")
            #print(record)
            #print("-----------------")
            socketio.emit("send_production_order", record, room=room)
        else:
            print("PRODUCTION ORDER NOT FOUND !")
            socketio.emit("send_production_order", {}, room=room)
            socketio.emit("console", "No Production Order", room=room)
            time.sleep(0.5)


@app.route('/downtime_details/<orders>')
def downtime_details_history(orders):
    if 'user' in session and 'login' in session:
        if session['login']:

            res = orders.split(",")
            order_left = res[0]
            order_right = res[1]


            return render_template("downtime_analysis_history.html",

                                   role=session['role'],
                                   name=session['name'],
                                   order_number_left = order_left,
                                   order_number_right = order_right,
                                   phone=session["user"]

                                   )
    else:
        return redirect(url_for('login'))


@app.route('/rejection_details/<orders>')
def rejection_details_history(orders):
    if 'user' in session and 'login' in session:
        if session['login']:
            res = orders.split(",")
            order_left = res[0]
            order_right = res[1]

            return render_template("rejection_analysis_history.html",

                                   role=session['role'],
                                   name=session['name'],
                                   order_number_left=order_left,
                                   order_number_right=order_right
                                   )
    else:
        return redirect(url_for('login'))


@app.route('/ROHIT', methods=['GET', 'POST'])
def rohit():
    if request.method == 'POST':
        print("RECIEVED FROM ROHIT $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        #data = request.get_json(force=True)
        data = request.get_json(force=True)
        new_data = str(data)
        print(new_data)


        myjson = {"name":"Rohit"}

    return "ok received"

#==================================end======================================================================

if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", port=5002, debug=True)
    # socketio.run(host="0.0.0.0", port=5001, debug=True,
    #         ssl_context=('www.cyronicssolutions.com.pem', 'www.cyronicssolutions.com.key'))
     #socketio.run(app, host="127.0.0.1", port=5000, debug=True)


