import pandas
import numpy as np
import pymongo

'''
Include all scripts
'''

import os
import sys
sys.path.append(os.path.join("..","Production Planning Sheets"))

excel_data_df = pandas.read_excel('database_30_june_2021.XLSX', sheet_name='I ST QTR',index_col=None ,skiprows=[0] )

excel_data_df = excel_data_df.replace(np.nan, '', regex=True)
excel_data_df = excel_data_df.replace('\n','', regex=True)


database_dict = excel_data_df.to_dict(orient='record')

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["PPAP_OEE"]
production_order_parameters = mydb["production_order_parameters"]
production_order_parameters.drop()
production_order_parameters.insert_many(database_dict)

print("Production Order Database Ready ....")


