from datetime import datetime
import requests
import json

url = 'http://192.168.1.35/downtime_post'




time_format = "%d/%m/%Y, %H:%M:%S"
downtime_in_progress = 0
current_downtime = {}


f = open("operation_log.txt", "r")
data = f.read()
new_data = data.split("\n")
#print(new_data)
new_data = new_data[1:]

full_log = []
downtime = []

for record in new_data:
    temp_data = {}
    fields = record.split(",")

    if len(fields) > 1 :
        record_log = {}
        record_log['timestamp'] = datetime.strptime(fields[0], "%Y-%m-%dT%H:%M:%S") #fields[0]
        record_log['timestamp'] = record_log["timestamp"].strftime(time_format)
        record_log["Status"] = fields[1]
        record_log["Note"] = fields[2]


        if (fields[1] == "arrange" or fields[1] == "down") and downtime_in_progress == 0:
            downtime_in_progress = 1
            record_log["Downtime"] = "Start"
            current_downtime["start_time"] = datetime.strptime(fields[0], "%Y-%m-%dT%H:%M:%S")
            current_downtime['start_time'] = current_downtime["start_time"].strftime(time_format)
            current_downtime["details"] = fields[2]
            current_downtime["reasonlist"] = "Software"

        elif (fields[1] == "stop" or fields[1] == "production") and downtime_in_progress == 1:
            record_log["Downtime"] = "Stop"
            downtime_in_progress = 0
            current_downtime["stop_time"] = datetime.strptime(fields[0], "%Y-%m-%dT%H:%M:%S")
            current_downtime['stop_time'] = current_downtime["stop_time"].strftime(time_format)
            current_downtime["Mode"] = "Software"
            current_downtime["status"] = "Accept"
            downtime.append(current_downtime)
            current_downtime = {}

        else :
            record_log["Downtime"] = ""
        full_log.append(record_log)

print(downtime[1:5])



downtime_json={
    "timestamp" : datetime.now().strftime(time_format),
    "machine":"JSW 450T-III",
    "data" : [downtime[2]]
}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post(url, data=json.dumps(downtime_json), headers=headers)
# extracting response text
pastebin_url = r.text
print("The pastebin URL is:%s" % pastebin_url)


#print(len(downtime))

'''import csv
csv_columns = ['timestamp','Status','Note','Downtime']
csv_file = "Full_Log.csv"
try:
    with open(csv_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for data in full_log:
            writer.writerow(data)
except IOError:
    print("I/O error")'''

'''# Import pandas
import pandas as pd
import numpy as np

# reading csv file
excel_data_df = pd.read_csv("record1.csv")
excel_data_df = excel_data_df.replace(np.nan, '', regex=True)
excel_data_df = excel_data_df.replace('\n','', regex=True)



database_dict = excel_data_df.to_dict(orient='record')
print(database_dict[0])'''