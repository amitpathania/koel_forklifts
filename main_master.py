from flask import Flask, render_template, url_for, send_file, jsonify,request,session,send_from_directory,redirect
from flask_socketio import SocketIO, emit,join_room,leave_room
from flask_session import Session
import time
import datetime
from werkzeug.middleware.proxy_fix import ProxyFix


app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)
#socketio = SocketIO(app, manage_session=False)
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1)
socketio = SocketIO(app, manage_session=False,cors_allowed_origins="*")

import master_database_operations as database_operations
import otp_operations


@socketio.on("connected")
def connected():
    print("connected")

@socketio.on("Log In")
def Log_In():
    print("Log In")


@socketio.on("Sign_Up_Request")
def sign_up_request(data):
    room =  data["phone"]
    join_room(room)
    session["user"] = data["phone"]
    session['role'] = data["role"]
    session['name'] = data["name"]
    session['o_id'] = data['o_id']
    session['login'] = False
    print(session)
    Val = database_operations.signup_user(data)
    if Val == True :
        socketio.emit("sign_up_status","Redirecting ...",room=room)
    else :
        socketio.emit("sign_up_status","This name and number already exists in organisation.",room=room)



@socketio.on("verify_otp_data")
def verify_otp_data(data):
    print("In verify otp data")
    if 'user' in session :
        phone = session['user']
        room = session['user']
        join_room(room)
        otp = database_operations.return_otp(phone)
        if str(otp['otp']) == str(data) :
            socketio.emit("otp_status","Redirecting ...",room=room)
            print("OTP successful")
        else:
            print("OTP not succesful but user found")
            socketio.emit("otp_status", "Verification not successful.",room=room)
        leave_room(room)
    else :
        room2 = request.remote_addr
        join_room(room2)
        print("OTP not succesful")
        socketio.emit("otp_status", "Verification not successful.",room=room2)
        leave_room(room2)


@socketio.on("resend_otp")
def resend_otp():
    if 'user' in session :
        phone = session['user']
        print("Found User")
        Val = database_operations.update_otp(phone)
        print("Update requested")
    else :
        room2 = request.remote_addr
        join_room()
        socketio.emit("otp_status", "Verification not successful.",room=room2)
        leave_room(room2)

@socketio.on("login_cred")
def login_cred(data):
    room =  data['phone']
    join_room(room)
    print(data)
    Val = database_operations.verify_credentials(data)
    if Val == 2:
        session["user"] = data["phone"]
        session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = True
        socketio.emit("login_status","2",room=room)
        leave_room(room)
    if Val == 1:
        session["user"] = data["phone"]
        session['role'] = data["role"]
        data1 = database_operations.return_credentials(data['phone'])
        session['name'] = data1['name']
        session['o_id'] = data1['o_id']
        session["login"] = False
        socketio.emit("login_status", "1",room=room)
    if Val == 0 :
        socketio.emit("login_status", "0",room=room)


@socketio.on("forgot_generate_otp")
def forgot_generate_otp(phone):
    room = phone
    join_room(phone)
    Val = database_operations.update_otp(phone)
    print("Update requested")
    leave_room(phone)
    #Send otp

@socketio.on("forgot_verify_otp")
def forgot_verify_otp(data):
    room = data["phone"]
    join_room(room)
    otp = database_operations.return_otp(data['phone'])
    if str(otp['otp']) == str(data['otp']):
        socketio.emit("otp_status", "Redirecting ...",room=room)
        print("OTP successful")
        session['user'] = data['phone']
        leave_room(room)
    else:
        print("OTP not succesful but user found")
        socketio.emit("otp_status", "Verification not successful.",room=room)

@socketio.on("reset_password")
def forgot_verify_otp(password):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data={}
        data['phone'] = session['user']
        data['password'] = password
        print("Found User")
        Val = database_operations.reset_password(data)
        if Val == True:
            socketio.emit("update_pass_status", "True",room=room)
            leave_room(room)
        else:
            socketio.emit("update_pass_status", "False",room=room)


@socketio.on("logout")
def logout():

    if 'user' in session :
        session.pop('user')
        session.pop("role")
        session.pop("name")
        session.pop('o_id')
        session.pop('login')
        print("Logged Out")
    else :
        print("User not found !")

@socketio.on("declaration_data")
def declaration(data):
    print(data)
    if 'user' in session:
        phone = session['user']
        room = phone
        join_room(room)
        Val = database_operations.update_declaration(phone,data)
        time.sleep(1)
        socketio.emit("declaration_submitted",room=room)
        #leave_room(room)


@app.route('/template')
def template():
    return render_template("template.html")

# ===================================================

@app.route('/visitor')
def visitor():
    return render_template("visitor.html")

@app.route('/visitor_epass')
def visitor_epass():
    return render_template("visitor_epass.html")

@socketio.on("visitor_Request")
def visitor_Request(dt):
    client = request.sid
    join_room(client)
    data = database_operations.visitor_request(dt)
    #socketio.emit("application",data)

@app.route('/temperature')
def temperature():
    if 'user' in session and 'login' in session:
        if session['login']:
            user_temp=database_operations.retrive_user_temp(session['user'])
            user_flag = True
            if len(user_temp) == 1:
                user_flag = False
            return render_template("temperature.html",
                                   active_page='temperature',
                                   role=session['role'],
                                   name=session['name'],
                                   user_temp=user_temp,
                                   user_flag = user_flag
                                   )
    else:
        return redirect(url_for('login'))



# ===================================================


@app.route('/epass/<path:filename>')
def download_file(filename):
    print('correct loop')
    return send_from_directory('code', filename, as_attachment=True)

@app.route('/login')
def login():
    if 'user' in session :
        phone = session['user']
        credentials = database_operations.return_credentials(phone)
        return render_template("signin.html",phone=phone,password=credentials['password'],
                               role=credentials['role'],o_id=credentials['o_id'])
    else :
        return render_template("signin.html")

@app.route('/signup')
def signup():
    questions = database_operations.get_signup_questions()
    return render_template("signup.html",questions=questions)

@app.route('/verifyotp')
def verifyotp():
    if 'user' in session :
        print(session['user'])
        return render_template("verifyotp.html",user=session["user"])
    else :
        return render_template("signup.html")

@app.route('/forgotpassword')
def forgotpassword():
    return render_template("forgotpassword.html")


@app.route('/newpassword')
def newpassword():
    if 'user' in session :
        return render_template("newpassword.html")
    else :
        return render_template("signup.html")

@app.route('/epass')
def epass():
    if 'user' in session and 'login' in session:
        if session['login']:
            color="info"
            phone = session['user']
            epass = database_operations.get_epass(phone)
            #print(epass)
            timestamp = ""
            button_text = ""
            if "timestamp" in  epass :
                timestamp ="Last record on : "+  epass['timestamp'].strftime("%d/%m/%Y %H:%M:%S")
                if (datetime.datetime.now() + datetime.timedelta(minutes=330) - epass['timestamp'] ) > datetime.timedelta(1):
                    #print("1 day elapsed")
                    epass['epass_status']="No Application found in last 24 hours."
                    epass['qrcode_link'] = '/epass/fillup.jpg'
                    #print(timestamp)
                    color = "warning"
                    button_text = "Apply for E-Pass"
                if epass['epass_status'] == 'Accepted' :
                    color = "success"
                    button_text = "Re-apply for E-Pass"
                if epass['epass_status'] == 'Rejected':
                    color = "danger"
                    button_text = "Retry for E-Pass"
                #print(color)
            else :
                epass['epass_status'] = "You do not have any E-Pass Applications. Please answer questions in Self-Declaration section to get an E-Pass"
                epass['qrcode_link'] = '/epass/fillup.jpg'
                button_text = "Go to Self-Declaration "
            return render_template("epass.html",status=epass['epass_status'],
                                   last_app=timestamp,
                                   src=epass["qrcode_link"],
                                   color=color,
                                   active_page ='epass',
                                   role=session['role'],
                                   name=session['name'],
                                   button_text = button_text
                                   )
    else :
        return redirect(url_for('login'))


@app.route('/declaration')
def declaration():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("declaration.html",
                                   active_page='declaration',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else :
        return redirect(url_for('login'))

@app.route('/distancing')
def distancing():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("distancing.html",
                                   active_page='distancing',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else :
        return redirect(url_for('login'))


@app.route('/notifications')
def notifications():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("notifications.html",
                               active_page='notifications',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))


@app.route('/security')
def secrutiy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("security.html",
                               active_page='security',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))


@app.route('/manualsecurity')
def manualsecrutiy():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("manual_security.html",
                               active_page='security',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))

@app.route('/contact')
def contact():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("contact.html",
                               active_page='contact',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))

@app.route('/admin')
def admin ():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("admin.html",
                               active_page='admin',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))


# --------------------------------------trace ----------------------------------
@app.route('/trace')
def trace():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("trace.html",
                               active_page='admin',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))

@socketio.on("trace")
def trace_data(input):
    if 'user' in session and 'login' in session:
        if session['login']:
            room = session['user']
            join_room(room)
            val,data = database_operations.trace_data(input,session['o_id'])
            if val == True :
                socketio.emit("trace_data",data,room=room)
            else :
                socketio.emit("trace_data_status", "Employee not found", room=room)

#-----------------------------attendance--------------------------------------------


@app.route('/attendance')
def attendance():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("attendance.html",
                                   active_page='attendance',
                                   role=session['role'],
                                   name=session['name'],
                                   )
    else:
        return redirect(url_for('login'))


@socketio.on("attendance_data")
def attendance_data():
    if 'user' in session :
        room = session['user']
        join_room(room)

        current_time = datetime.datetime.now() + datetime.timedelta(minutes=330)
        date = current_time.strftime("%d/%m/%Y")
        socketio.emit("set_date",date , room=room)

        data = database_operations.attendance_data(session['o_id'])
        socketio.emit("reciv_attendance",data,room=room)

@socketio.on("modal_attendance")
def modal_attendance(data):
    if 'user' in session :
        room = session['user']
        join_room(room)
        data = database_operations.attendance_modal(data,session['o_id'])
        socketio.emit("modal_data",data,room=room)

@socketio.on("date_wise_attendance")
def date_wise_attendance(date):
    if 'user' in session :
        room = session['user']
        join_room(room)

        socketio.emit("set_date", date, room=room)

        data = database_operations.date_wise_attendance(date,session['o_id'])
        socketio.emit("reciv_attendance",data,room=room)



#------------------------------visitors-----------------------------------

@app.route('/visitors')
def visitors():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("visitors.html",
                               active_page='visitors',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))


@socketio.on("visitor_data")
def visitor_data():
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.visitor_data(session['o_id'])
        socketio.emit("reciv_visitor",data,room=room)


@socketio.on("modal_visitor")
def modal_visitor(no):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.visitor_modal(no,session['o_id'])
        socketio.emit("modal_data_visitor",data,room=room)

@socketio.on("date_wise_visitor")
def date_wise_visitor(date):
    if 'user' in session:
        room = session['user']
        join_room(room)
        data = database_operations.date_wise_visitor(date,session['o_id'])
        socketio.emit("reciv_visitor",data,room=room)


#--------------------------------------------Applications-----------------------

@app.route('/applications')
def application():
    if 'user' in session and 'login' in session:
        if session['login']:
            return render_template("application.html",
                               active_page='applications',
                               role=session['role'],
                               name=session['name'],
                               )
    else:
        return redirect(url_for('login'))

@socketio.on("application_data")
def application_data():
    if 'user' in session:
        room = session['user']
        join_room(room)

        data = database_operations.application_data(session['o_id'])
        socketio.emit("application",data,room=room)

@socketio.on("change_status")
def change_status(arr):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.change_status(arr,session['o_id'])
        socketio.emit("application",data,room=room)

@socketio.on("question")
def cquestion(question):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.log_question(question,session)
        #socketio.emit("application",data,room=room)

@socketio.on("modal_application")
def modal_application(x):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        data = database_operations.modal_application(x,session['o_id'])
        socketio.emit("modal_data_application",data,room=room)

@socketio.on("updated_role")
def updated_role(arr):
    if 'user' in session:
        room = session["user"]
        data = database_operations.update_role(arr,session['o_id'])
        return "ok"

@socketio.on("manual_security")
def manual_security(data):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        val,temp_ok = database_operations.manual_temp_entry(data,session)
        if val == False :
            socketio.emit("manual_scurity_status","not_a_person",room = room)
        else :
            if temp_ok == False :
                socketio.emit("manual_scurity_status", "temp_high", room=room)
            if temp_ok == True :
                socketio.emit("manual_scurity_status", "temp_ok", room=room)
            #print(data)

@socketio.on("scanned_security")
def scanned_security(data):
    if 'user' in session:
        room = session["user"]
        join_room(room)
        val,temp_ok = database_operations.scanned_security(data,session)
        if val == False :
            socketio.emit("manual_scurity_status","not_a_person",room = room)
        else :
            if temp_ok == False :
                socketio.emit("manual_scurity_status", "temp_high", room=room)
            if temp_ok == True :
                socketio.emit("manual_scurity_status", "temp_ok", room=room)
            #print(data)

#-------------------------




#-------------------------

#===================++++++++++++++++somnath code end here++++++++++++===============================#
if __name__ == "__main__":
    socketio.run(app,host="0.0.0.0",port=5000,debug=True)
    #socketio.run(app,ssl_context=('cert.pem', 'key.pem'),host='192.168.1.65', debug=True)

