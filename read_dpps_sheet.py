import pandas as pd
import pprint

import pandas
import numpy as np
import pymongo
import copy
import json

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["PPAP_OEE"]
production_order_parameters = mydb["production_order_parameters"]

'''
Include all scripts
'''

'''import os
import sys
sys.path.append(os.path.join("..","Production Planning Sheets"))'''


def read_excel(filename, sheetname):
    try:
        # columns_to_parse = ['A','B','C','E','F ', 'G',"H","I","J","K","l","M","N","O","P","V","X","AC","AD","AF","AG"]
        excel_data_df = pandas.read_excel(filename, sheet_name=sheetname, skiprows=[0], index_col=None)
    except:
        return "Sheet Name entered incorrectly. Please check !", None, None, None, None

    excel_data_df = excel_data_df.replace(np.nan, '', regex=True)
    excel_data_df = excel_data_df.replace('\n', '', regex=True)


    try:
        excel_data_df = excel_data_df[[
            'PART NO.',
            'PART NAME',
            'CHILD PART 1',
            'CHILD PART 2',
            'CHILD PART 3',
            'CHILD PART 4',
            'CHILD PART 5',
            'CHILD PART 6',
            'CHILD PART 7',
        ]]
    except:
        return "Sheet structure Incorrect. Please Check !", None, None, None, None

    print(excel_data_df.columns.values.tolist())
    database_dict = excel_data_df.to_dict(orient='record')

    print("--------- Starting Database Operations! --------")

    part_not_found_list = []
    part_found_list = []

    for j in range (len(database_dict)) :
        child_parts = database_dict[j]

        myquery = { "PART NUMBER" :  child_parts['PART NO.'] }
        record = production_order_parameters.find_one(myquery, {'_id': False})

        if bool(record):

            DA_TAPE = ""
            TAB_TAPE = ""
            child_parts_list = []

            for i in range(1,8):
                temp = "CHILD PART " + str(i)
                part = child_parts[temp]
                #print(temp + " : " + part)

                if part.__contains__("D/A TAPE"):
                    DA_TAPE = part
                elif part.__contains__("TAB TAPE"):
                    TAB_TAPE = part
                elif part == "":
                    pass
                else :
                    child_parts_list.append(part)

            record["TAB_TAPE"] = TAB_TAPE
            record["DA_TAPE"] = DA_TAPE
            record["child_parts"] = child_parts_list

            myquery = {"PART NUMBER" :  child_parts['PART NO.']}
            newvalues = {"$set": record}
            production_order_parameters.update_one(myquery, newvalues)
            part_found_list.append( child_parts['PART NO.'] )
            #print(json.dumps(record, indent=4))
        else :
            part_not_found_list.append( child_parts['PART NO.'] )

    print( "PART NUMBERS NOT FOUND IN DATABASE : ", len(part_not_found_list) )
    print("PART NUMBERS FOUND IN DATABASE: ", len(part_found_list))

    try :
        print(part_found_list)
    except:
        pass





if __name__ == '__main__':
    read_excel("DPPS.xlsx", "20")
